unit Unit7;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Menus, ExtCtrls;

type

  { Tcp_form }

  Tcp_form = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    ComboBox4: TComboBox;
    ComboBox5: TComboBox;
    GroupBox1: TGroupBox;
    Image1: TImage;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure ComboBox1Select(Sender: TObject);
    procedure ComboBox2Select(Sender: TObject);
    procedure ComboBox3Select(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure ComboBox4Select(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  cp_form: Tcp_form;
  time2: integer;

implementation
uses Unit5,Unit6,Unit10,Unit1;

{$R *.lfm}

{ Tcp_form }

procedure Tcp_form.Button1Click(Sender: TObject);
begin
  application.CreateForm(TTest_Redactor_form,Test_Redactor_form);
  hide;
  Test_Redactor_form.show;
end;

procedure Tcp_form.Button2Click(Sender: TObject);
var ip_global_last: string;
begin
  if messagedlg('Удалить тест?','Вы действительно хотите удалить этот тест?'+
  slinebreak+'Данные невозможно будет восстановить.',mtwarning,[mbyes,mbno],0)=
  mryes then
  begin
    ip_global_last:=GetIP;
    Auth.MyQuery.SQL.Clear;
    Auth.MyQuery.SQL.add('INSERT INTO table_log (ip_global,user_id,action,id_table) VALUES(:ip_global,:user_id,:action,:id_table)');
    Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global_last;
    Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
    Auth.MyQuery.Params.ParamByName('action').value:='Удаление';
    Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
    bd_log;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('UPDATE tests SET del=:del,active=:active WHERE id=:id_table');
    Auth.SQLQuery1.Params.ParamByName('id_table').value:=id_table;
    Auth.SQLQuery1.Params.ParamByName('del').value:='yes';
    Auth.SQLQuery1.Params.ParamByName('active').value:='no';
    Auth.SQLQuery1.ExecSQL;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    //self.Repaint;
  end;
end;

procedure Tcp_form.Button3Click(Sender: TObject);
begin
  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT MAX(id) FROM tests');
  Auth.SQLQuery1.Open;
  id_table:=Auth.SQLQuery1.FieldByName('MAX(id)').AsInteger+1;
  Auth.MySQL56Connection1.Connected:=false;
  Auth.SQLQuery1.SQL.Clear;
  application.CreateForm(TTest_Redactor_form,Test_Redactor_form);
  Test_Redactor_form.Button1.Caption:='Создать';
  Test_Redactor_form.Show;
  hide;
end;

procedure Tcp_form.ComboBox1Select(Sender: TObject);
begin
  if combobox1.ItemIndex>0 then
  begin
    combobox4.items.clear;
    combobox4.text:='Поиск по названию';
    button2.Enabled:=false;
    button1.Enabled:=false;
    combobox2.Items.clear;
    combobox2.Items.add('Выберите группу...');
    combobox2.ItemIndex:=0;
    combobox3.Items.clear;
    combobox3.Items.add('Выберите тест...');
    combobox3.ItemIndex:=0;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
    Auth.SQLQuery1.ExecSQL;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('SELECT * FROM groups WHERE kyrs=:kyrs');
    Auth.SQLQuery1.Params.ParamByName('kyrs').value:=combobox1.Items[combobox1.ItemIndex];
    Auth.SQLQuery1.Open;
    while not Auth.SQLQuery1.EOF do
    begin
      ComboBox2.Items.Add(Auth.SQLQuery1.FieldByName('group').AsString);
      Auth.SQLQuery1.Next;
    end;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
  end
  else
  begin
    combobox2.Items.clear;
    combobox2.Items.add('Выберите группу...');
    combobox2.ItemIndex:=0;
    combobox3.Items.clear;
    combobox3.Items.add('Выберите тест...');
    combobox3.ItemIndex:=0;
    button2.Enabled:=false;
    button1.Enabled:=false;
  end;
end;

procedure Tcp_form.ComboBox2Select(Sender: TObject);
begin
  if combobox2.ItemIndex>0 then
  begin
    combobox4.items.clear;
    combobox4.text:='Поиск по названию';
    combobox3.items.clear;
    combobox3.Items.add('Выберите тест...');
    combobox3.ItemIndex:=0;
    button2.Enabled:=false;
    button1.Enabled:=false;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    if not (user_flags='admin') then
    begin
      Auth.SQLQuery1.SQL.add('SELECT * FROM tests WHERE user_group like :group and del=:del');
      Auth.SQLQuery1.Params.ParamByName('del').value:='no';
    end
    else
    Auth.SQLQuery1.SQL.add('SELECT * FROM tests WHERE user_group like :group');
    Auth.SQLQuery1.Params.ParamByName('group').value:='%'+combobox2.Items[combobox2.ItemIndex]+'%';
    Auth.SQLQuery1.Open;
    while not Auth.SQLQuery1.EOF do
    begin
      ComboBox3.Items.Add(Auth.SQLQuery1.FieldByName('Name').AsString);
      Auth.SQLQuery1.Next;
    end;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    ComboBox3.visible:=true;
  end
  else
  begin
    combobox3.Items.clear;
    combobox3.Items.add('Выберите тест...');
    combobox3.ItemIndex:=0;
    button2.Enabled:=false;
    button1.Enabled:=false;
  end;
end;

procedure Tcp_form.ComboBox3Select(Sender: TObject);
begin
  if combobox3.ItemIndex>0 then
  begin
    button1.enabled:=true;
    combobox4.items.clear;
    combobox4.text:='Поиск по названию';
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('SELECT * FROM tests WHERE name=:name');
    Auth.SQLQuery1.Params.ParamByName('name').value:=combobox3.Items[combobox3.ItemIndex];
    Auth.SQLQuery1.Open;
    id_table:=Auth.SQLQuery1.FieldByName('id').AsInteger;
    Auth.MySQL56Connection1.Connected:=false;
    Auth.SQLQuery1.SQL.Clear;
    button2.Enabled:=true;
  end
  else
  begin
    button2.Enabled:=false;
    button1.Enabled:=false;
  end;
end;

procedure Tcp_form.ComboBox4Change(Sender: TObject);
begin
  timer1.enabled:=true;
  time2:=1;
end;

procedure Tcp_form.ComboBox4Select(Sender: TObject);
begin
  id_table:=resume_test[combobox4.itemindex,0,0];
  combobox1.ItemIndex:=0;
  combobox2.Items.clear;
  combobox2.Items.add('Выберите группу...');
  combobox2.ItemIndex:=0;
  combobox3.Items.clear;
  combobox3.Items.add('Выберите тест...');
  combobox3.ItemIndex:=0;
  Button1.Enabled:=true;
  Button2.Enabled:=true;
end;

procedure Tcp_form.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  mainmenu_form.show;
end;

procedure Tcp_form.Timer1Timer(Sender: TObject);
var i: integer;
begin
  time2:=time2-1;
  if (combobox4.text<>'') and (time2=0) then
  begin
    timer1.enabled:=false;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    if not (user_flags='admin') then
    begin
      Auth.SQLQuery1.SQL.add('SELECT * FROM tests WHERE name like :name and del=:del');
      Auth.SQLQuery1.Params.ParamByName('del').value:='no';
    end
    else
    Auth.SQLQuery1.SQL.add('SELECT * FROM tests WHERE name like :name');
    Auth.SQLQuery1.Params.ParamByName('name').value:='%'+combobox4.text+'%';
    Auth.SQLQuery1.Open;
    combobox4.items.clear;
    SetLength(resume_test,Auth.SQLQuery1.RecordCount,1);
    i:=0;
    while not Auth.SQLQuery1.EOF do
    begin
      resume_test[i,0,0]:=Auth.SQLQuery1.FieldByName('id').AsInteger;
      combobox4.items.add(Auth.SQLQuery1.FieldByName('name').AsString);
      i:=i+1;
      Auth.SQLQuery1.Next;
    end;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
  end
  else if combobox4.text='' then
  begin
    Button1.Enabled:=false;
    Button2.Enabled:=false;
  end;
end;

end.
