unit thread;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs, LazUTF8, Forms;

type
  TMyThread = class(TThread)
  private
  protected
    procedure Execute; override;
  public
    property Terminated;
  end;

var user_check: boolean;

implementation

uses Unit1, Unit12;

procedure TMyThread.Execute;
var ip_global,ip_local,computer_name,err: string;
begin
  user_check:=false;
  GetIPFromHost(computer_name,ip_local,err);
  ip_global:=GetIP;
  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
  Auth.SQLQuery1.ExecSQL;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT * FROM users WHERE login=:login');
  Auth.SQLQuery1.Params.ParamByName('login').value:=login;
  Auth.SQLQuery1.Open;
  if UTF16ToUTF8(Trim(Serial))=Auth.SQLQuery1.FieldByName('serial').value then
  user_check:=true
  else if (Auth.GetMacAddress()=Auth.SQLQuery1.FieldByName('mac_reg').value) and
  ((ip_global=Auth.SQLQuery1.FieldByName('ip_global_reg').value) or
  (ip_local=Auth.SQLQuery1.FieldByName('ip_local_reg').value) or
  (computer_name=Auth.SQLQuery1.FieldByName('computer_name_reg').value)) then
  begin
    Auth.MySQL56Connection1.Connected:=false;
    account_confirm.show;
    while (account_confirm.visible=true) and (pass_check=false) do
    begin
      Application.ProcessMessages;
    end;
    if pass_check=false then
    messagedlg('Отмена подтверждения','Подтверждение действия было отменено!',mtwarning,[mbok],0)
    else
    user_check:=true;
    account_confirm.release;
  end
  else
  begin

  end;
end;

end.
