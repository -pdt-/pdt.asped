unit Unit11;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { Tinformation }

  Tinformation = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  information: Tinformation;

implementation

{$R *.lfm}

{ Tinformation }

procedure Tinformation.Button1Click(Sender: TObject);
begin
  information.release;
end;

end.

