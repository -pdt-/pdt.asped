unit Unit9;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IdHTTP, FileUtil, RichMemo, Forms, Controls, Graphics,
  Dialogs, StdCtrls, Buttons, ComCtrls, IdComponent, IdAntiFreeze,
  CommCtrl, windows;

type

  { TUpdate_form }

  TUpdate_form = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    IdAntiFreeze1: TIdAntiFreeze;
    IdHTTP1: TIdHTTP;
    Label1: TLabel;
    Label2: TLabel;
    ProgressBar1: TProgressBar;
    RichMemo1: TRichMemo;
    StatusBar1: TStatusBar;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Update_form: TUpdate_form;
  Filename,file1: string;

implementation
uses Unit1;

{$R *.lfm}

{ TUpdate_form }

function ExtractOnlyFileName(const FileName: string): string;
begin
  result:=StringReplace(ExtractFileName(FileName),ExtractFileExt(FileName),'',[]);
end;

procedure TUpdate_form.FormClose(Sender: TObject; var CloseAction: TCloseAction
  );
begin
  Update_form.Release;
  Auth.Show;
end;

procedure TUpdate_form.IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
  ProgressBar1.Position:=AWorkCount;
end;

procedure TUpdate_form.IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
begin
  ProgressBar1.Position:=0;
  ProgressBar1.Max:=AWorkcountMax;
end;

procedure TUpdate_form.BitBtn2Click(Sender: TObject);
begin
  Update_form.Release;
  Auth.Show;
end;

procedure TUpdate_form.BitBtn3Click(Sender: TObject);
begin
  BitBtn3.Enabled:=false;
  IdHTTP1.Disconnect;
  IdHTTP1.Socket.Close;
  BitBtn1.Enabled:=true;
  BitBtn2.enabled:=true;
  StatusBar1.panels[0].text:='Отмена обновления';
  ProgressBar1.visible:=false;
  ProgressBar1.style:=pbstNormal;
end;

procedure TUpdate_form.BitBtn1Click(Sender: TObject);
var FS: TFileStream;
  f: textfile;
begin
  BitBtn1.Enabled:=false;
  BitBtn2.enabled:=false;
  BitBtn3.visible:=true;
  BitBtn3.enabled:=true;
  ProgressBar1.visible:=true;
  file1:=ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'_new.exe';
  Filename:=ExtractFilePath(ParamStr(0))+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'_new.exe';
  FS:=TFileStream.Create(Filename, fmCreate);
  try
    IdHTTP1.Get(update_URL, FS);
    FS.Free;
    if BitBtn1.Enabled=true then
    //SysUtils.DeleteFile(file1)
    else
    begin
      AssignFile(f,ExtractFilePath(ParamStr(0))+'Updater.bat');
      ReWrite(f);
      WriteLn(f,'@echo off');
      WriteLn(f,'chcp 1251 >nul');
      writeln(f, ':1');
      writeln(f, 'erase '+ExtractShortPathName(Application.ExeName));
      writeln(f, 'if exist '+ExtractShortPathName(Application.ExeName)+' Goto 1');
      WriteLn(f,'rename '+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'.exe'+' '+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'_old.*');
      WriteLn(f,'rename '+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'_new.exe '+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'.*');
      WriteLn(f,'del '+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'_old.exe');
      WriteLn(f,'start "" "'+ExtractOnlyFileName(ExtractFileName(ParamStr(0)))+'.exe"');
      WriteLn(f,'del "%~f0"');
      CloseFile(f);
      ShellExecute(0,nil, PChar('cmd'),PChar('/c Updater.bat'),nil,1);
      application.terminate;
    end;
  except
    IdHTTP1.Disconnect;
    IdHTTP1.Socket.Close;
    SysUtils.DeleteFile(file1);
    StatusBar1.panels[0].text:='Ошибка обновления';
    ProgressBar1.style:=pbstMarquee;
  end;
end;

end.

