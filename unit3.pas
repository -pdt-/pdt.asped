unit Unit3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, IdSMTP, IdMessage, IdSSLOpenSSL, FileUtil, IdAntiFreeze,
  Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls, ExtCtrls, IdText,
  IdExplicitTLSClientServerBase, IdCoderHeader, IdComponent, MD5, LCLProc;

type

  { Tconfirm_auth }

  Tconfirm_auth = class(TForm)
    confirm_button: TButton;
    confirmedit: TEdit;
    IdAntiFreeze1: TIdAntiFreeze;
    IdMessage1: TIdMessage;
    IdSMTP1: TIdSMTP;
    IdSSLIOHandlerSocketOpenSSL1: TIdSSLIOHandlerSocketOpenSSL;
    Label1: TLabel;
    no_mail: TLabel;
    StatusBar: TStatusBar;
    procedure confirm_buttonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure IdMessage1InitializeISO(var VHeaderEncoding: Char;
      var VCharSet: string);
    procedure mail_confirm_send;
    procedure no_mailClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  confirm_auth: Tconfirm_auth;
  confirm_code,session_ID: string;

implementation
uses Unit1, Unit4, Unit5;

{$R *.lfm}

{ Tconfirm_auth }

procedure Tconfirm_auth.mail_confirm_send;
var txtpart, htmpart: TIdText;
  ip_global: string;
  err: integer;
begin
  err:=0;
  StatusBar.panels[0].text:='Попытка отправки письма';
  randomize;
  confirm_code:=inttostr(random(10))+inttostr(random(10))+inttostr(random(10))+
  inttostr(random(10))+inttostr(random(10))+inttostr(random(10))+inttostr(random(10))+
  inttostr(random(10))+inttostr(random(10))+inttostr(random(10));
  session_ID:=inttostr(random(11))+inttostr(random(11))+inttostr(random(11))+
  inttostr(random(11))+inttostr(random(11));
  StatusBar.panels[1].text:='ID сессии: '+session_ID;
  try
    try
      IdSMTP1.Host := 'smtp.yandex.ru';
      IdSMTP1.Port := 465;
      IdSMTP1.Username := 'gintr1k@pixeldeveloperteam.ru';
      IdSMTP1.Password := 'a100804Utmz';
      IdSMTP1.AuthType := satDefault;
      IdSMTP1.UseTLS := utUseImplicitTLS;
      IdSMTP1.Connect();
    except
      StatusBar.panels[0].text:='Не удалось подключиться к yandex';
      err:=1;
    end;

    IdMessage1.Clear();
    IdMessage1.Recipients.EMailAddresses:=mail;// на какой адрес выслать сообщение
    IdMessage1.From.Address:='gintr1k@pixeldeveloperteam.ru';
    IdMessage1.From.Name:=EncodeHeader('PixelDeveloperTeam.ru','C','Q','windows-1251');
    IdMessage1.ContentType := 'multipart/alternative';
    IdMessage1.Subject:='EMail Verefication - PixelDeveloperTeam.ru';

    txtpart := TIdText.Create(IdMessage1.MessageParts);
    txtpart.ContentType := 'text/plain; charset=UTF-8';

    txtpart := TIdText.Create(IdMessage1.MessageParts);
    txtpart.ContentType := 'multipart/related; type="text/html"';

    htmpart := TIdText.Create(IdMessage1.MessageParts, nil);
    htmpart.ContentType := 'text/html; charset=UTF-8';
    htmpart.Body.Add('<html>');
    htmpart.Body.Add('<head>');
    htmpart.Body.Add('</head>');
    htmpart.Body.Add('<body><h3>Ваш уникальный код:</h3>');
    htmpart.Body.Add('<h2>'+pass_gen(confirm_code)+'</h2>');
    htmpart.Body.Add('Данный код актуален для сессии '+session_ID+'<br/>');
    htmpart.Body.Add('--'+'<br/>');
    htmpart.Body.Add('PixelDeveloperTeam.ru - Задачи по дисциплине "География"');
    htmpart.Body.Add('</body>');
    htmpart.Body.Add('</html>');
    htmpart.ParentPart := 1;

    IdSMTP1.Send(confirm_auth.IdMessage1);
    IdSMTP1.Disconnect();
    StatusBar.panels[0].text:='Письмо было отправлено!';
  except
    StatusBar.panels[0].text:='Неверный почтовый адрес';
    IdSMTP1.Disconnect();
    err:=1;
  end;
  ip_global:=GetIP;
  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('INSERT INTO confirm_logs (user_id,action,correct_code,session_id,ip_global,mail) VALUES (:user_id,:action,:correct_code,:session_id,:ip_global,:mail)');
  Auth.SQLQuery1.Params.ParamByName('user_id').value:=user_id;
  Auth.SQLQuery1.Params.ParamByName('ip_global').value:=ip_global;
  Auth.SQLQuery1.Params.ParamByName('session_ID').value:=session_ID;
  Auth.SQLQuery1.Params.ParamByName('correct_code').value:=confirm_code;
  if err=0 then
  Auth.SQLQuery1.Params.ParamByName('action').value:='Выслан код подтверждения'
  else
  Auth.SQLQuery1.Params.ParamByName('action').value:='Произошла ошибка: '+StatusBar.panels[0].text;
  Auth.SQLQuery1.Params.ParamByName('mail').value:=mail;
  Auth.SQLQuery1.ExecSQL;
  Auth.SQLQuery1.SQL.Clear;
  Auth.MySQL56Connection1.Connected:=false;
end;

procedure Tconfirm_auth.no_mailClick(Sender: TObject);
begin
  Application.CreateForm(Tno_mail_form, no_mail_form);
  no_mail_form.show;
  confirm_auth.hide;
end;

procedure Tconfirm_auth.confirm_buttonClick(Sender: TObject);
var ip_global: string;
  entered_code: string[32];
begin
  confirm_button.Enabled:=false;
  ip_global:=GetIP;
  Auth.MyQuery.SQL.Clear;
  Auth.MyQuery.SQL.add('INSERT INTO confirm_logs (user_id,action,correct_code,session_id,ip_global,mail,entered_code) VALUES (:user_id,:action,:correct_code,:session_id,:ip_global,:mail,:entered_code)');
  Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
  Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
  Auth.MyQuery.Params.ParamByName('session_ID').value:=session_ID;
  Auth.MyQuery.Params.ParamByName('correct_code').value:=confirm_code;
  Auth.MyQuery.Params.ParamByName('mail').value:=mail;
  entered_code:=confirmedit.text;
  if UTF8Length(confirmedit.text)<=32 then
  begin
    Auth.MyQuery.Params.ParamByName('entered_code').value:=entered_code;
    if confirmedit.text=pass_gen(confirm_code) then
    begin
      Auth.MyQuery.Params.ParamByName('action').value:='Аккаунт подтвержден';
      bd_log;
      try
        Auth.MySQL56Connection1.Connected:=true;
        Auth.SQLQuery1.SQL.Clear;
        Auth.SQLQuery1.SQL.add('UPDATE users SET confirmed=1 WHERE login=:login');
        Auth.SQLQuery1.Params.ParamByName('login').value:=login;
        Auth.SQLQuery1.ExecSQL;
      except
        Auth.SQLQuery1.SQL.Clear;
        Auth.MySQL56Connection1.Connected:=false;
        messagedlg('Ошибка отправки запроса!',mterror,[mbok],0);
        exit;
      end;
      Auth.SQLQuery1.SQL.Clear;
      Auth.MySQL56Connection1.Connected:=false;
      variant_user:=2;
      Application.CreateForm(TMainMenu_Form, MainMenu_Form);
      MainMenu_Form.show;
      confirm_auth.Release;
      Auth.hide;
    end
    else
    begin
      StatusBar.panels[0].text:='Ошибка подтверждения! Код введен неверно!';
      Auth.MyQuery.Params.ParamByName('entered_code').value:=entered_code;
      Auth.MyQuery.Params.ParamByName('action').value:='Ошибка: Код введен неверно';
      bd_log;
      messagedlg('Ошибка подтверждения','Введенный код не соответствует отправленному на почту!'
      ,mterror,[mbok],0);
    end;
  end
  else
  begin
    StatusBar.panels[0].text:='Ошибка подтверждения! Код введен неверно!';
    Auth.MyQuery.Params.ParamByName('entered_code').value:=entered_code;
    Auth.MyQuery.Params.ParamByName('action').value:='Ошибка: Код больше 32 символов';
    bd_log;
    messagedlg('Ошибка подтверждения','Введенный код не соответствует отправленному на почту!',
    mterror,[mbok],0);
  end;
  confirm_button.Enabled:=true;
end;

procedure Tconfirm_auth.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  confirm_auth.Release;
  Auth.show;
end;

procedure Tconfirm_auth.IdMessage1InitializeISO(var VHeaderEncoding: Char;
  var VCharSet: string);
begin
  VHeaderEncoding := 'B';
  VCharSet := 'UTF-8';
end;

end.

