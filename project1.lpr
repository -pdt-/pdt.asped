program project1;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Unit1, zcomponent, laz_synapse, datetimectrls, Unit2, Unit3, Unit4,
  Unit5, Unit6, Unit7, Unit8, Unit9, Unit10, Unit11, Unit12, Unit13, Consts;

{$R *.res}

begin
  Application.Title:='Автоматизированная система '
    +'проверки и контроля знаний';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TAuth, Auth);
  Application.Run;
end.

