#!/bin/bash

LOG=ssh.log
rm $LOG
trap bashtrap 0

bashtrap()
{
    echo "ssh kill started at" >> $LOG
    date >> $LOG
    kill -9 $pid
    echo "Amen" >> $LOG
}

export SSH_ASKPASS="./pass.sh" # ���� ����������� ������, ������� ������ ������ � STDOUT
export DISPLAY=":0" # ���� ���� ���� � ��� ���� X

setsid ssh -L 10000:localhost:3306 denis@server sleep INF &
pid=$!
echo "ssh pid=$pid" >> $LOG

sleep INF