unit Unit12;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { Taccount_confirm }

  Taccount_confirm = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Passedit: TEdit;
    Label1: TLabel;
    Timer: TTimer;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  account_confirm: Taccount_confirm;
  pass_check: boolean;

implementation
uses Unit1;

{$R *.lfm}

{ Taccount_confirm }

procedure Taccount_confirm.FormCreate(Sender: TObject);
begin
  ShowModal;
  Passedit.passwordchar:=#149;
  pass_check:=false;
end;

procedure Taccount_confirm.Button1Click(Sender: TObject);
var password: string;
  const err_pass: integer = 0;
begin
  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
  Auth.SQLQuery1.ExecSQL;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT * FROM users WHERE login=:login and password=:password');
  Auth.SQLQuery1.Params.ParamByName('login').value:=login;
  password:=pass_gen(passedit.text);
  Auth.SQLQuery1.Params.ParamByName('password').value:=password;
  Auth.SQLQuery1.Open;
  if Auth.SQLQuery1.recordcount=1 then
  begin
    pass_check:=true;
    Auth.MySQL56Connection1.Connected:=false;
    Auth.SQLQuery1.SQL.Clear;
  end
  else
  begin
    err_pass:=err_pass+1;
    passedit.Clear;
    if err_pass>2 then
    begin
      Timer.enabled:=true;
      if (error_login_count mod 5 > 1) and (error_login_count mod 5 < 5) then
      messagedlg('Вы ошиблись при вводе пароля '+
      inttostr(error_login_count)+' раза. '+
      'Доступ заблокирован на '+inttostr(time_login_count)+
      ' секунд',mtwarning,[mbok],0)
      else
      messagedlg('Вы ошиблись при вводе пароля '+
      inttostr(error_login_count)+' раз. Доступ'+' заблокирован на '+
      inttostr(time_login_count)+' секунд',mtwarning,[mbok],0);
    end
    else
    messagedlg('Логин или пароль не найден!',mterror,[mbok],0);
  end;
end;

procedure Taccount_confirm.FormDeactivate(Sender: TObject);
begin

end;

procedure Taccount_confirm.TimerTimer(Sender: TObject);
begin
  passedit.enabled:=false;
  time_login_count:=time_login_count-1;
  passedit.PasswordChar:=#0;
  passedit.text:=inttostr(time_login_count);
  if time_login_count=0 then
  begin
    time_login_count:=10;
    Timer.Enabled:=false;
    passedit.enabled:=true;
    passedit.PasswordChar:=#149;
  end;
end;

end.

