unit Unit8;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Clipbrd;

type

  { TRedactor_form }

  TRedactor_form = class(TForm)
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    CheckBox1: TCheckBox;
    CheckBox10: TCheckBox;
    CheckBox11: TCheckBox;
    CheckBox12: TCheckBox;
    CheckBox13: TCheckBox;
    CheckBox14: TCheckBox;
    CheckBox15: TCheckBox;
    CheckBox16: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    CheckBox9: TCheckBox;
    DateTimePicker1: TDateTimePicker;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LabeledEdit8: TLabeledEdit;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    LabeledEdit5: TLabeledEdit;
    LabeledEdit6: TLabeledEdit;
    LabeledEdit7: TLabeledEdit;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    RadioButton7: TRadioButton;
    RadioButton8: TRadioButton;
    Button1: TButton;
    Memo1: TMemo;
    StaticText1: TStaticText;
    procedure BitBtn4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure CheckBox2Change(Sender: TObject);
    procedure CheckBox3Change(Sender: TObject);
    procedure CheckBox4Change(Sender: TObject);
    procedure CheckBox5Change(Sender: TObject);
    procedure CheckBox6Change(Sender: TObject);
    procedure CheckBox7Change(Sender: TObject);
    procedure CheckBox8Change(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit1KeyPress(Sender: TObject; var Key: char);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure save_changes;
    procedure StaticText1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Redactor_form: TRedactor_form;
  good_answer: string;
  Arr: array[1..8] of TLabeledEdit;
  CheckBoxes: array[1..8] of TCheckBox;
  RadioButtons: array[1..8] of TRadioButton;
  CheckBox: array[1..8] of TCheckBox;

implementation
uses Unit6, Unit1, Unit10;

{$R *.lfm}

{ TRedactor_form }

procedure TRedactor_form.save_changes;
var i: integer;
  TS : TStringList;
  ip_global: string;
begin
  if label1.Enabled=true then
  for i:=1 to 8 do
  begin
    if RadioButtons[i].checked=true then
    good_answer:=inttostr(i);
  end
  else
  begin
    good_answer:='';
    for i:=1 to 8 do
    begin
      if checkbox[i].checked=true then
      good_answer:=good_answer+inttostr(i)+',';
    end;
    SetLength(good_answer,Length(good_answer)-1);
  end;
  try
    Auth.MySQL56Connection1.Connected:=true;
  except
    messagedlg('Ошибка подключения к БД!',mterror,[mbok],0);
    exit;
  end;
  Auth.SQLQuery1.SQL.Clear;
  TS := TStringList.Create;
  TS.Text := Memo1.Text;
  for i:=TS.Count-1 downto 0 do
  if Length(trim(TS.Strings[i]))=0
  then TS.Delete(i);
  Memo1.Text:=TS.Text;
  Auth.SQLQuery1.SQL.add('UPDATE test'+inttostr(id_table)+' SET text=:text,test_type=:type,good_answer=:good_answer,bal=:bal,time=:time,');
  for i:=1 to 8 do
  begin
    if i=8 then
    begin
      Auth.SQLQuery1.SQL.add('answer'+inttostr(i)+'=:answer'+inttostr(i));
      Auth.SQLQuery1.SQL.add(' WHERE id_test=:id_test');
      Auth.SQLQuery1.Params.ParamByName('id_test').value:=id_test;
    end
    else
    Auth.SQLQuery1.SQL.add('answer'+inttostr(i)+'=:answer'+inttostr(i)+',');
  end;
  for i:=1 to 8 do
  if (Arr[i].Enabled) and (Arr[i].text<>'') then
  Auth.SQLQuery1.Params.ParamByName('answer'+inttostr(i)).value:=Arr[i].text
  else
  Auth.SQLQuery1.Params.ParamByName('answer'+inttostr(i)).value:=NULL;
  Auth.SQLQuery1.Params.ParamByName('text').value:=Memo1.Lines.text;
  Auth.SQLQuery1.Params.ParamByName('good_answer').value:=good_answer;
  if DateTimePicker1.Checked=true then
  Auth.SQLQuery1.Params.ParamByName('time').value:=FormatDateTime('HH:mm',DateTimePicker1.Time)
  else
  Auth.SQLQuery1.Params.ParamByName('time').value:=null;
  if label2.Enabled=false then
  Auth.SQLQuery1.Params.ParamByName('type').value:='radio'
  else
  Auth.SQLQuery1.Params.ParamByName('type').value:='checkbox';
  Auth.SQLQuery1.Params.ParamByName('bal').value:=Edit1.text;
  Auth.MyQuery.SQL.Clear;
  Auth.MyQuery.SQL.add('INSERT INTO test_log (user_id,ip_global,test_id,id_table,action,test_type,text,good_answer,time_limit,');
  for i:=1 to 7 do
  Auth.MyQuery.SQL.add('answer'+inttostr(i)+',');
  Auth.MyQuery.SQL.add('answer8,bal) VALUES(:user_id,:ip_global,:test_id,:id_table,:action,:test_type,:text,:good_answer,:time_limit,');
  for i:=1 to 7 do
  Auth.MyQuery.SQL.add(':answer'+inttostr(i)+',');
  Auth.MyQuery.SQL.add(':answer8,:bal); ');
  ip_global:=GetIP;
  Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
  Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
  Auth.MyQuery.Params.ParamByName('test_id').value:=id_test;
  Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
  Auth.MyQuery.Params.ParamByName('action').value:='Редактирование';
  if DateTimePicker1.Checked=true then
  Auth.MyQuery.Params.ParamByName('time_limit').value:=FormatDateTime('HH:mm',DateTimePicker1.Time)
  else
  Auth.MyQuery.Params.ParamByName('time_limit').value:=null;
  for i:=1 to 8 do
  if (Arr[i].Enabled) and (Arr[i].text<>'') then
  Auth.MyQuery.Params.ParamByName('answer'+inttostr(i)).value:=Arr[i].text
  else
  Auth.MyQuery.Params.ParamByName('answer'+inttostr(i)).value:=NULL;
  Auth.MyQuery.Params.ParamByName('text').value:=Auth.SQLQuery1.Params.ParamByName('text').value;
  Auth.MyQuery.Params.ParamByName('good_answer').value:=Auth.SQLQuery1.Params.ParamByName('good_answer').value;
  Auth.MyQuery.Params.ParamByName('bal').value:=Auth.SQLQuery1.Params.ParamByName('bal').value;
  Auth.MyQuery.Params.ParamByName('test_type').value:=Auth.SQLQuery1.Params.ParamByName('type').value;
  bd_log;


  Auth.SQLQuery1.ExecSQL;
  Auth.SQLQuery1.SQL.Clear;
  Auth.MySQL56Connection1.Connected:=false;
  messagedlg('Задание бновлено!','Задание было успешно обновлено!',mtcustom,[mbok],0);
end;

procedure TRedactor_form.StaticText1Click(Sender: TObject);
var ip_global: string;
begin
  if messagedlg('Восстановить задание?','Вы действительно хотите восстановить данное задание?',mtinformation,
  [mbyes,mbno],0)=mryes then
  begin
    Auth.MyQuery.SQL.Clear;
    Auth.MyQuery.SQL.add('INSERT INTO test_log (user_id,ip_global,id_table,test_id,action) VALUES(:user_id,:ip_global,:id_table,:test_id,:action)');
    ip_global:=GetIP;
    Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
    Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
    Auth.MyQuery.Params.ParamByName('test_id').value:=id_test;
    Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
    Auth.MyQuery.Params.ParamByName('action').value:='Восстановление';
    bd_log;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('UPDATE test'+inttostr(id_table)+' SET del=:del WHERE id_test=:id_test');
    Auth.SQLQuery1.Params.ParamByName('del').value:='no';
    Auth.SQLQuery1.Params.ParamByName('id_test').value:=id_test;
    Auth.SQLQuery1.ExecSQL;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    StaticText1.Visible:=false;
  end;
end;

procedure TRedactor_form.FormCreate(Sender: TObject);
var i: integer;
begin
  Arr[1]:=LabeledEdit1;
  Arr[2]:=LabeledEdit2;
  Arr[3]:=LabeledEdit3;
  Arr[4]:=LabeledEdit4;
  Arr[5]:=LabeledEdit5;
  Arr[6]:=LabeledEdit6;
  Arr[7]:=LabeledEdit7;
  Arr[8]:=LabeledEdit8;
  CheckBoxes[1]:=CheckBox1;
  CheckBoxes[2]:=CheckBox2;
  CheckBoxes[3]:=CheckBox3;
  CheckBoxes[4]:=CheckBox4;
  CheckBoxes[5]:=CheckBox5;
  CheckBoxes[6]:=CheckBox6;
  CheckBoxes[7]:=CheckBox7;
  CheckBoxes[8]:=CheckBox8;
  CheckBox[1]:=CheckBox9;
  CheckBox[2]:=CheckBox10;
  CheckBox[3]:=CheckBox11;
  CheckBox[4]:=CheckBox12;
  CheckBox[5]:=CheckBox13;
  CheckBox[6]:=CheckBox14;
  CheckBox[7]:=CheckBox15;
  CheckBox[8]:=CheckBox16;
  RadioButtons[1]:=RadioButton1;
  RadioButtons[2]:=RadioButton2;
  RadioButtons[3]:=RadioButton3;
  RadioButtons[4]:=RadioButton4;
  RadioButtons[5]:=RadioButton5;
  RadioButtons[6]:=RadioButton6;
  RadioButtons[7]:=RadioButton7;
  RadioButtons[8]:=RadioButton8;
  if button1.caption='Сохранить' then
  begin
  try
    Auth.MySQL56Connection1.Connected:=true;
  except
    messagedlg('Ошибка подключения к БД!',mterror,[mbok],0);
    exit;
  end;
  try
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('SELECT *,DATE_FORMAT(time,''%H:%i'') FROM test'+inttostr(id_table)+' WHERE id_test=:id_test');
    Auth.SQLQuery1.Params.ParamByName('id_test').value:=id_test;
    try
      Auth.SQLQuery1.Open;
    except
      Auth.SQLQuery1.SQL.Clear;
      Auth.MySQL56Connection1.Connected:=false;
      messagedlg('Ошибка получения таблицы!',mterror,[mbok],0);
      exit;
    end;
    if Auth.SQLQuery1.FieldByName('del').value='yes' then
    StaticText1.Visible:=true
    else
    StaticText1.Visible:=false;
    if Auth.SQLQuery1.FieldByName('DATE_FORMAT(time,''%H:%i'')').AsString<>'' then
    begin
      DateTimePicker1.Time:=strtotime(Auth.SQLQuery1.FieldByName('DATE_FORMAT(time,''%H:%i'')').AsString);
      DateTimePicker1.Checked:=true;
    end
    else
    DateTimePicker1.Checked:=false;
    Memo1.text:=Auth.SQLQuery1.FieldByName('text').AsString;
    Memo1.Text:=DelLastEnter(Memo1.Text);
    Edit1.Text:=Auth.SQLQuery1.FieldByName('bal').AsString;
    good_answer:=Auth.SQLQuery1.FieldByName('good_answer').value;
    if Auth.SQLQuery1.FieldByName('test_type').value='radio' then
    begin
      label1.Enabled:=true;
      label2.enabled:=false;
      for i:=1 to 8 do
      begin
        checkbox[i].Visible:=false;
        RadioButtons[i].Visible:=true;
        if i=strtoint(good_answer) then
        RadioButtons[i].Checked:=true;
      end;
    end
    else
    begin
      label2.Enabled:=true;
      label1.enabled:=false;
      for i:=1 to 8 do
      begin
        CheckBox[i].Visible:=true;
        RadioButtons[i].Visible:=false;
        if pos(inttostr(i),good_answer)<>0 then
        CheckBox[i].checked:=true;
      end;
    end;
    for i:=1 to 8 do
    begin
      if Auth.SQLQuery1.FieldByName('answer'+inttostr(i)).AsString <> '' then
      begin
        Arr[i].Text:=Auth.SQLQuery1.FieldByName('answer'+inttostr(i)).AsString;
        CheckBoxes[i].checked:=true;
      end
      else
      begin
        Arr[i].editlabel.caption:=Arr[i].editlabel.caption+' - выключен';
        Arr[i].Enabled:=false;
        RadioButtons[i].Enabled:=false;
      end;
    end;
  except
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    messagedlg('Ошибка отправки запроса!',mterror,[mbok],0);
    exit;
  end;
  Auth.SQLQuery1.SQL.Clear;
  Auth.MySQL56Connection1.Connected:=false;
end
  else
  begin
    for i:=1 to 8 do
    begin
      CheckBox[i].visible:=true;
      CheckBox1Change(self);
      CheckBox2Change(self);
      CheckBox3Change(self);
      CheckBox4Change(self);
      CheckBox5Change(self);
      CheckBox6Change(self);
      CheckBox7Change(self);
      CheckBox8Change(self);
    end;
    DelLastEnter(Memo1.Text);
    label1.Enabled:=false;
  end;
end;

procedure TRedactor_form.Label1Click(Sender: TObject);
var i: integer;
begin
  label2.Enabled:=true;
  label2.Font.Style:=[fsUnderline];
  label1.enabled:=false;
  label1.Font.Style:=[];
  for i:=1 to 8 do
  begin
    checkbox[i].Visible:=true;
    CheckBox[i].Enabled:=true;
    RadioButtons[i].Visible:=false;
    if RadioButtons[i].Checked=true then
    begin
      checkbox[i].Checked:=true;
      RadioButtons[i].Checked:=false;
    end;
  end;
end;

procedure TRedactor_form.Label2Click(Sender: TObject);
var i: integer;
begin
  label1.Enabled:=true;
  label1.Font.Style:=[fsUnderline];
  label2.enabled:=false;
  label2.Font.Style:=[];
  for i:=1 to 8 do
  begin
    checkbox[i].Visible:=false;
    RadioButtons[i].Visible:=true;
    if checkbox[i].Checked=true then
    begin
      RadioButtons[i].Checked:=true;
      checkbox[i].Checked:=false;
    end;
  end;
end;

procedure TRedactor_form.Button1Click(Sender: TObject);
var i: integer;
  ip_global: string;
begin
  Memo1.Text:=DelLastEnter(Memo1.Text);
  if DateTimePicker1.Checked=true then
  begin
    if Round(DateTimePicker1.Time*SecsPerDay)<600 then
    begin
      messagedlg('Ошибка сохранения!','Минимальное время прохождение заданий - 10 сек',mterror,[mbok],0);
      exit;
    end;
  end;
  if Button1.caption='Сохранить' then
  save_changes
  else
  if Button1.caption='Создать' then
  begin
    if label1.Enabled=true then
    for i:=1 to 8 do
    begin
      if RadioButtons[i].checked=true then
      good_answer:=inttostr(i);
    end
    else
    begin
      good_answer:='';
      for i:=1 to 8 do
      begin
        if checkbox[i].checked=true then
        good_answer:=good_answer+inttostr(i)+',';
      end;
      SetLength(good_answer,Length(good_answer)-1);
    end;
    Auth.MyQuery.SQL.Clear;
    Auth.MyQuery.SQL.add('INSERT INTO test_log (user_id,ip_global,test_id,id_table,action,test_type,text,good_answer,time_limit,');
    for i:=1 to 7 do
    Auth.MyQuery.SQL.add('answer'+inttostr(i)+',');
    Auth.MyQuery.SQL.add('answer8,bal) VALUES(:user_id,:ip_global,:test_id,:id_table,:action,:test_type,:text,:good_answer,:time_limit,');
    for i:=1 to 7 do
    Auth.MyQuery.SQL.add(':answer'+inttostr(i)+',');
    Auth.MyQuery.SQL.add(':answer8,:bal); ');
    ip_global:=GetIP;
    Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
    Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
    Auth.MyQuery.Params.ParamByName('test_id').value:=id_test;
    Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
    Auth.MyQuery.Params.ParamByName('action').value:='Создание задания';
    for i:=1 to 8 do
    if (Arr[i].Enabled) and (Arr[i].text<>'') then
    Auth.MyQuery.Params.ParamByName('answer'+inttostr(i)).value:=Arr[i].text
    else
    Auth.MyQuery.Params.ParamByName('answer'+inttostr(i)).value:=NULL;
    Auth.MyQuery.Params.ParamByName('text').value:=Memo1.Lines.text;
    Auth.MyQuery.Params.ParamByName('good_answer').value:=good_answer;
    Auth.MyQuery.Params.ParamByName('bal').value:=Edit1.Text;
    if label1.Enabled=true then
    Auth.MyQuery.Params.ParamByName('test_type').value:='radio'
    else
    Auth.MyQuery.Params.ParamByName('test_type').value:='checkbox';
    if DateTimePicker1.Checked=true then
    Auth.MyQuery.Params.ParamByName('time_limit').value:=FormatDateTime('hh:mm',DateTimePicker1.Time)
    else
    Auth.MyQuery.Params.ParamByName('time_limit').value:=null;
    bd_log;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('INSERT INTO test'+inttostr(id_table)+' (test_type,text,good_answer,time,');
    for i:=1 to 7 do
    Auth.SQLQuery1.SQL.add('answer'+inttostr(i)+',');
    Auth.SQLQuery1.SQL.add('answer8,bal) VALUES(:test_type,:text,:good_answer,:time,');
    for i:=1 to 7 do
    Auth.SQLQuery1.SQL.add(':answer'+inttostr(i)+',');
    Auth.SQLQuery1.SQL.add(':answer8,:bal); ');
    for i:=1 to 8 do
    if (Arr[i].Enabled) and (Arr[i].text<>'') then
    Auth.SQLQuery1.Params.ParamByName('answer'+inttostr(i)).value:=Arr[i].text
    else
    Auth.SQLQuery1.Params.ParamByName('answer'+inttostr(i)).value:=NULL;
    Auth.SQLQuery1.Params.ParamByName('text').value:=Memo1.Lines.text;
    Auth.SQLQuery1.Params.ParamByName('good_answer').value:=good_answer;
    if label1.Enabled=true then
    Auth.SQLQuery1.Params.ParamByName('test_type').value:='radio'
    else
    Auth.SQLQuery1.Params.ParamByName('test_type').value:='checkbox';
    Auth.SQLQuery1.Params.ParamByName('bal').value:=Edit1.Text;
    if DateTimePicker1.Checked=true then
    Auth.SQLQuery1.Params.ParamByName('time').value:=FormatDateTime('hh:mm',DateTimePicker1.Time)
    else
    Auth.SQLQuery1.Params.ParamByName('time').value:=null;
    Auth.SQLQuery1.ExecSQL;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    messagedlg('Успех!','Задание успешно создано!',mtinformation,[mbok],0);
  end;
  close;
end;

procedure TRedactor_form.BitBtn4Click(Sender: TObject);
begin
  close;
end;

procedure TRedactor_form.CheckBox1Change(Sender: TObject);
begin
  if CheckBox1.Checked=false then
  CheckBox1.Checked:=true;
end;

procedure TRedactor_form.CheckBox2Change(Sender: TObject);
begin
  if CheckBox2.Checked=false then
  CheckBox2.Checked:=true;
end;

procedure TRedactor_form.CheckBox3Change(Sender: TObject);
begin
  if CheckBox3.Checked then
  begin
    LabeledEdit3.enabled:=true;
    RadioButton3.Enabled:=true;
    CheckBox11.Enabled:=true;
    LabeledEdit3.editlabel.caption:='Ответ №3';
  end;
  if CheckBox3.Checked=false then
  begin
    RadioButton3.Enabled:=false;
    LabeledEdit3.enabled:=false;
    CheckBox11.Checked:=false;
    CheckBox11.enabled:=false;
    LabeledEdit3.editlabel.caption:='Ответ №3 - выключен';
  end;
end;

procedure TRedactor_form.CheckBox4Change(Sender: TObject);
begin
  if CheckBox4.Checked then
  begin
    LabeledEdit4.enabled:=true;
    RadioButton4.Enabled:=true;
    CheckBox12.Enabled:=true;
    LabeledEdit4.editlabel.caption:='Ответ №4';
  end;
  if CheckBox4.Checked=false then
  begin
    LabeledEdit4.enabled:=false;
    RadioButton4.Enabled:=false;
    CheckBox12.Checked:=false;
    CheckBox12.enabled:=false;
    LabeledEdit4.editlabel.caption:='Ответ №4 - выключен';
  end;
end;

procedure TRedactor_form.CheckBox5Change(Sender: TObject);
begin
  if CheckBox5.Checked then
  begin
    LabeledEdit5.enabled:=true;
    RadioButton5.Enabled:=true;
    CheckBox13.Enabled:=true;
    LabeledEdit5.editlabel.caption:='Ответ №5';
  end;
  if CheckBox5.Checked=false then
  begin
    LabeledEdit5.enabled:=false;
    RadioButton5.Enabled:=false;
    CheckBox13.Checked:=false;
    CheckBox13.enabled:=false;
    LabeledEdit5.editlabel.caption:='Ответ №5 - выключен';
  end;
end;

procedure TRedactor_form.CheckBox6Change(Sender: TObject);
begin
  if CheckBox6.Checked then
  begin
    LabeledEdit6.enabled:=true;
    RadioButton6.Enabled:=true;
    CheckBox14.Enabled:=true;
    LabeledEdit6.editlabel.caption:='Ответ №6';
  end;
  if CheckBox6.Checked=false then
  begin
    LabeledEdit6.enabled:=false;
    RadioButton6.Enabled:=false;
    CheckBox14.Checked:=false;
    CheckBox14.enabled:=false;
    LabeledEdit6.editlabel.caption:='Ответ №6 - выключен';
  end;
end;

procedure TRedactor_form.CheckBox7Change(Sender: TObject);
begin
  if CheckBox7.Checked then
  begin
    LabeledEdit7.enabled:=true;
    CheckBox15.Enabled:=true;
    RadioButton7.Enabled:=true;
    LabeledEdit7.editlabel.caption:='Ответ №7';
  end;
  if CheckBox7.Checked=false then
  begin
    LabeledEdit7.enabled:=false;
    RadioButton7.Enabled:=false;
    CheckBox15.Checked:=false;
    CheckBox15.enabled:=false;
    LabeledEdit7.editlabel.caption:='Ответ №7 - выключен';
  end;
end;

procedure TRedactor_form.CheckBox8Change(Sender: TObject);
begin
  if CheckBox8.Checked then
  begin
    LabeledEdit8.enabled:=true;
    RadioButton8.Enabled:=true;
    CheckBox16.Enabled:=true;
    LabeledEdit8.editlabel.caption:='Ответ №8';
  end;
  if CheckBox8.Checked=false then
  begin
    CheckBox16.Checked:=false;
    CheckBox16.enabled:=false;
    LabeledEdit8.enabled:=false;
    RadioButton8.Enabled:=false;
    LabeledEdit8.editlabel.caption:='Ответ №8 - выключен';
  end;
end;

procedure TRedactor_form.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if ((ssCtrl in Shift) AND (Key = ord('V'))) or (Key = 45) then
  begin
    if Clipboard.HasFormat(CF_TEXT) then
    ClipBoard.Clear;
    Key := 0;
  end;
end;

procedure TRedactor_form.Edit1KeyPress(Sender: TObject; var Key: char);
begin
  if not (key in ['0'.. '9',#8]) then
  key:= #0;
end;

procedure TRedactor_form.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  Test_redactor_form.show;
  redactor_form.release;
end;

end.

