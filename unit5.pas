unit Unit5;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls,
  Graphics, Dialogs, StdCtrls, ExtCtrls, Buttons, lclintf, types;

type

  { TMainMenu_form }

  TMainMenu_form = class(TForm)
    BitBtn1: TBitBtn;
    admin_button: TButton;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Memo1: TMemo;
    Open_Test: TButton;
    CP_Button: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    hello_text: TLabel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    ListBox1: TListBox;
    procedure admin_buttonClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Open_TestClick(Sender: TObject);
    procedure CP_ButtonClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure Label2Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  MainMenu_form: TMainMenu_form;

implementation
uses Unit6,Unit1,Unit8,Unit7,Unit11,Unit13;

{$R *.lfm}

{ TMainMenu_form }

procedure TMainMenu_form.Open_TestClick(Sender: TObject);
begin
  if ListBox1.ItemIndex>-1 then
  begin
    id_table:=resume_test[ListBox1.ItemIndex,0,0];
    id_test:=resume_test[ListBox1.ItemIndex,1,0];
    time1:=0;
    MainMenu_form.hide;
    Application.CreateForm(Ttest_form, test_form);
    test_form.show;
  end;
end;

procedure TMainMenu_form.BitBtn1Click(Sender: TObject);
begin
  application.createform(TInformation, information);
  information.show;
end;

procedure TMainMenu_form.admin_buttonClick(Sender: TObject);
begin
  //if showmessage
end;

procedure TMainMenu_form.Button4Click(Sender: TObject);
begin
  Application.CreateForm(Tresult_form, result_form);
  result_form.showmodal;
end;

procedure TMainMenu_form.Button5Click(Sender: TObject);
begin

end;

procedure TMainMenu_form.FormShow(Sender: TObject);
begin
  MainMenu_reload;
end;

procedure TMainMenu_form.CP_ButtonClick(Sender: TObject);
begin
  Application.CreateForm(Tcp_form, cp_form);
  cp_form.show;
  MainMenu_form.hide;
end;

procedure TMainMenu_form.Button3Click(Sender: TObject);
begin

end;

procedure TMainMenu_form.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  application.terminate;
end;

procedure TMainMenu_form.Label2Click(Sender: TObject);
begin
  OpenUrl('https://pixeldeveloperteam.ru/');
end;

procedure TMainMenu_form.ListBox1Click(Sender: TObject);
begin
  if ListBox1.ItemIndex>-1 then
  Open_Test.enabled:=true;
end;

end.

