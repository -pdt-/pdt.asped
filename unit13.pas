unit Unit13;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
  StdCtrls;

type

  { Tresult_form }

  Tresult_form = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    StringGrid1: TStringGrid;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  result_form: Tresult_form;

implementation
uses Unit1;

{$R *.lfm}

{ Tresult_form }

procedure Tresult_form.Button1Click(Sender: TObject);
begin
  close;
end;

procedure Tresult_form.FormCreate(Sender: TObject);
var i: integer;
begin
  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT name,plus,minus,bal,bal1,mark FROM test_results WHERE user_id=:user_id');
  Auth.SQLQuery1.Params.ParamByName('user_id').value:=user_id;
  try
    Auth.SQLQuery1.Open;
  except
    Auth.MySQL56Connection1.Connected:=false;
    Auth.SQLQuery1.SQL.Clear;
    messagedlg('Ошибка!','Ошибка получения таблицы!'+slinebreak+'Попробуйте повторить попытку позже',mterror,[mbok],0);
  end;
  i:=1;
  while not Auth.SQLQuery1.EOF do
  begin
    StringGrid1.RowCount:=StringGrid1.RowCount+1;
    stringgrid1.Cells[0,i]:=Auth.SQLQuery1.FieldByName('name').AsString;
    stringgrid1.Cells[1,i]:=Auth.SQLQuery1.FieldByName('plus').AsString;
    stringgrid1.Cells[2,i]:=Auth.SQLQuery1.FieldByName('minus').AsString;
    stringgrid1.Cells[3,i]:=Auth.SQLQuery1.FieldByName('bal').AsString+'/'+Auth.SQLQuery1.FieldByName('bal1').AsString;
    stringgrid1.Cells[4,i]:=Auth.SQLQuery1.FieldByName('mark').AsString;
    i:=i+1;
    Auth.SQLQuery1.Next;
  end;
  Auth.MySQL56Connection1.Connected:=false;
  Auth.SQLQuery1.SQL.Clear;
end;

end.

