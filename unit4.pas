unit Unit4;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { Tno_mail_form }

  Tno_mail_form = class(TForm)
    Button1: TButton;
    send_mail_text: TLabel;
    New_Mail: TEdit;
    Label1: TLabel;
    No_mail_old: TLabel;
    Label3: TLabel;
    No_mailText: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure send_mail_textClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  no_mail_form: Tno_mail_form;

implementation
Uses Unit3, Unit1;

{$R *.lfm}

{ Tno_mail_form }

procedure Tno_mail_form.Button1Click(Sender: TObject);
var ip_global: string;
begin
  if New_mail.text<>'' then
  begin
    try
      Auth.MySQL56Connection1.Connected:=true;
    except
      messagedlg('Ошибка подключения к БД!',mterror,[mbok],0);
      exit;
    end;
    try
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
      Auth.SQLQuery1.ExecSQL;
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('SELECT * FROM users WHERE login=:login and mail=:mail');
      Auth.SQLQuery1.Params.ParamByName('login').value:=login;
      Auth.SQLQuery1.Params.ParamByName('mail').value:=mail;
      try
        Auth.SQLQuery1.Open;
      except
        Auth.SQLQuery1.SQL.Clear;
        Auth.MySQL56Connection1.Connected:=false;
        messagedlg('Ошибка получения таблицы!',mterror,[mbok],0);
        exit;
      end;
      if Auth.SQLQuery1.recordcount = 1 then
      begin
        mail:=new_mail.text;
        ip_global:=GetIP;
        Auth.SQLQuery1.SQL.Clear;
        Auth.MyQuery.SQL.add('INSERT INTO confirm_logs (user_id,action,correct_code,ip_global,mail) VALUES (:user_id,:action,:correct_code,:ip_global,:mail)');
        Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
        Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
        Auth.MyQuery.Params.ParamByName('correct_code').value:=confirm_code;
        Auth.MyQuery.Params.ParamByName('action').value:='Смена почты';
        Auth.MyQuery.Params.ParamByName('mail').value:=mail;
        bd_log;
        Auth.SQLQuery1.SQL.Clear;
        Auth.SQLQuery1.SQL.add('UPDATE users SET mail=:mail WHERE login=:login');
        Auth.SQLQuery1.Params.ParamByName('mail').value:=mail;
        Auth.SQLQuery1.Params.ParamByName('login').value:=login;
        Auth.SQLQuery1.ExecSQL;
        confirm_auth.mail_confirm_send;
      end;
    except
      Auth.SQLQuery1.SQL.Clear;
      Auth.MySQL56Connection1.Connected:=false;
      messagedlg('Ошибка отправки запроса!',mterror,[mbok],0);
      exit;
    end;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    no_mail_form.close;
  end
  else
  if messagedlg('Отменить ввод нового адреса почты?',
  mtConfirmation,[mbyes,mbno],0)=mryes then
  no_mail_form.release;
end;

procedure Tno_mail_form.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  confirm_Auth.show;
end;

procedure Tno_mail_form.FormCreate(Sender: TObject);
begin
  No_mail_old.Caption:=mail;
end;

procedure Tno_mail_form.send_mail_textClick(Sender: TObject);
begin
  no_mail_form.close;
  confirm_auth.mail_confirm_send;
end;

end.

