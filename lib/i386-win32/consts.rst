
# hash value = 236587980
consts.sopenfiletitle=#206#242#234#240#251#242#252

# hash value = 111898509
consts.scantwriteresourcestreamerror='Can'#39't write to a read-only reso'+
'urce stream'


# hash value = 89402421
consts.sduplicatereference='WriteObject called twice for the same instanc'+
'e'


# hash value = 13460193
consts.sclassmismatch=#208#229#241#243#240#241' %s - '#237#229#226#229#240+
#237#251#233' '#234#235#224#241#241

# hash value = 137225595
consts.sinvalidtabindex=#200#237#228#229#234#241' Tab'#39#224' '#226#251#245+
#238#228#232#242' '#231#224' '#227#240#224#237#232#246#251

# hash value = 43491136
consts.sinvalidtabposition=#207#238#231#232#246#232#255' Tab'#39#224' '#237+
#229#241#238#226#236#229#241#242#232#236#224' '#241' '#242#229#234#243#249+
#229#236' '#241#242#232#235#229#236' Tab'#39#224' '


# hash value = 86007905
consts.sinvalidtabstyle=#209#242#232#235#252' Tab'#39'a '#237#229#241#238+
#226#236#229#241#242#232#236' '#241' '#242#229#234#243#249#229#233' '#239+
#238#231#232#246#232#229#233' Tab'#39'a'


# hash value = 19958096
consts.sinvalidbitmap=#205#229#226#229#240#237#238#229' '#240#224#241#242+
#240#238#226#238#229'(Bitmap) '#232#231#238#225#240#224#230#229#237#232#229+
' '


# hash value = 138271833
consts.sinvalidicon=#205#229#226#229#240#237#238#229' '#232#231#238#225#240+
#224#230#229#237#232#229' '#232#234#238#237#234#232'(Icon)'


# hash value = 145741168
consts.sinvalidmetafile=#205#229#226#229#240#237#238#229' '#232#231#238#225+
#240#224#230#229#237#232#229' '#236#229#242#224#244#224#233#235#224

# hash value = 57468217
consts.sinvalidpixelformat=#205#229#226#229#240#237#251#233' '#244#238#240+
#236#224#242' '#239#232#234#241#229#235#229#233' (PixelFormat)'


# hash value = 184064101
consts.sinvalidimage=#205#229#226#229#240#237#238#229' '#232#231#238#225#240+
#224#230#229#237#232#229

# hash value = 80967333
consts.sbitmapempty=#207#243#241#242#238#229' '#232#231#238#225#240#224#230+
#229#237#232#229

# hash value = 150581814
consts.sscanline=#200#237#228#229#234#241' '#241#234#224#237#232#240#238#226+
#224#237#232#255' '#235#232#237#232#233' '#226#237#229' '#227#240#224#237+
#232#246

# hash value = 14184376
consts.schangeiconsize=#205#229#226#238#231#236#238#230#237#238' '#232#231+
#236#229#237#229#237#232#229' '#240#224#231#236#229#240#238#226' '#232#234+
#238#237#234#232

# hash value = 142007971
consts.solegraphic=#205#229#226#229#240#237#224#255' '#238#239#229#240#224+
#246#232#255' '#234' TOleGraphic'


# hash value = 201809513
consts.sunknownextension=#205#229#232#231#226#229#241#242#237#251#233' '#244+
#238#240#236#224#242' '#232#231#238#225#240#224#230#229#237#232#255' (.%s'+
')'


# hash value = 24861202
consts.sunknownclipboardformat=#205#229#239#238#228#228#229#240#230#232#226+
#224#229#236#251#233' '#225#243#244#229#240#238#236' '#244#238#240#236#224+
#242

# hash value = 134368875
consts.soutofresources=#194#237#229' '#240#229#241#243#240#241#238#226' '#241+
#232#241#242#229#236#251

# hash value = 117129356
consts.snocanvashandle=#202#224#237#226#224' (Canvas) '#237#229' '#239#238+
#231#226#238#235#255#229#242' '#240#232#241#238#226#224#242#252

# hash value = 46331855
consts.sinvalidimagesize=#205#229#226#229#240#237#251#233' '#240#224#231#236+
#229#240' '#232#231#238#225#240#224#230#229#237#232#255

# hash value = 3387689
consts.stoomanyimages=#209#235#232#248#234#238#236' '#236#237#238#227#238+
' '#232#231#238#225#240#224#230#229#237#232#233

# hash value = 95759236
consts.sdimsdonotmatch=#208#224#231#236#229#240#251' '#234#238#236#239#238+
#237#229#237#242#224' TImage '#237#229' '#241#238#238#242#226#229#242#241+
#242#226#243#254#242' '#240#224#231#236#229#240#224#236' TImageList'


# hash value = 92769380
consts.sinvalidimagelist=#205#229#226#229#240#237#251#233' ImageList'


# hash value = 16989519
consts.sreplaceimage=#205#229#228#238#241#242#243#239#237#224' '#241#236#229+
#237#224' '#232#231#238#225#240#224#230#229#237#232#255

# hash value = 68057716
consts.simageindexerror=#205#229#226#229#240#237#251#233' '#232#237#228#229+
#234#241' '#234#238#236#239#238#237#229#237#242#224' ImageList'


# hash value = 190566336
consts.simagereadfail=#206#248#232#225#234#224' '#247#242#229#237#232#255+
' '#228#224#237#237#251#245' ImageList '#232#231' '#239#238#242#238#234#224+


# hash value = 2305514
consts.simagewritefail=#206#248#232#225#234#224' '#231#224#239#232#241#232+
' '#228#224#237#237#251#245' ImageList '#226' '#239#238#242#238#234

# hash value = 264194800
consts.swindowdcerror=#206#248#232#225#234#224' '#241#238#231#228#224#237+
#232#255' '#234#238#237#242#229#234#241#242#224' '#238#234#237#224

# hash value = 26490973
consts.sclientnotset=#202#235#232#229#237#242' TDrag '#237#229' '#232#237+
#232#246#232#224#235#232#231#232#240#238#226#224#237

# hash value = 46042688
consts.swindowclass=#206#248#232#225#234#224' '#241#238#231#228#224#237#232+
#255' '#234#235#224#241#241#224' '#238#234#237#224

# hash value = 265911040
consts.swindowcreate=#206#248#232#225#234#224' '#241#238#231#228#224#237#232+
#255' '#238#234#237#224

# hash value = 99788757
consts.scannotfocus=#205#229#226#238#231#236#238#230#237#238' '#229#241#242+
#224#237#238#226#232#242#252' '#244#238#234#243#241' '#226#226#238#228#224+
' '#237#224' '#238#242#234#235#254#247#184#237#237#238#236' '#232#235#232+
' '#237#229#226#232#228#232#236#238#236' '#238#234#237#229

# hash value = 204118651
consts.sparentrequired='Control '#39'%s'#39' '#237#229' '#232#236#229#229+
#242' '#240#238#228#232#242#229#235#252#241#234#238#233' '#244#238#240#236+
#251

# hash value = 172069991
consts.sparentgivennotaparent=#207#229#240#229#228#224#237#237#251#233' '#240+
#238#228#232#242#229#235#252' '#237#229' '#240#238#228#232#242#229#235#252+
' '#39'%s'#39

# hash value = 57933593
consts.smdichildnotvisible=#205#229#226#238#231#236#238#230#237#238' '#241+
#239#240#255#242#224#242#252' '#228#238#247#229#240#237#254#254' '#244#238+
#240#236#243' MDI ('#236#237#238#227#238#228#238#234#243#236#229#237#242#237+
#238#227#238' '#239#240#232#235#238#230#229#237#232#255')'


# hash value = 62211701
consts.svisiblechanged=#209#226#238#233#241#242#226#238' Visible '#237#229+
#235#252#231#255' '#232#231#236#229#237#255#242#252' '#226' '#239#240#238+
#246#229#228#243#240#224#245' OnShow '#232#235#232' OnHide'


# hash value = 6937065
consts.scannotshowmodal=#204#238#228#224#235#252#237#238#229' '#238#234#237+
#238' '#237#229#235#252#231#255' '#241#228#229#235#224#242#252' '#226#232+
#228#232#236#251#236' (Visible:=true)'


# hash value = 53115574
consts.sscrollbarrange=#209#226#238#233#241#242#226#238' ScrollBar'#39#224+
' '#226#237#229' '#228#238#239#243#241#242#232#236#251#245' '#227#240#224+
#237#232#246

# hash value = 121489910
consts.spropertyoutofrange='%s '#241#226#238#233#241#242#226#238' '#226#237+
#229' '#228#238#239#243#241#242#232#236#251#245' '#227#240#224#237#232#246+


# hash value = 148442374
consts.smenuindexerror=#200#237#228#229#234#241' '#236#229#237#254' '#226+
#237#229' '#239#240#229#228#229#235#224#245' '#227#240#224#237#232#246

# hash value = 182352254
consts.smenureinserted=#204#229#237#254' '#228#226#224#230#228#251' '#226+
#241#242#224#226#235#229#237#238

# hash value = 138309790
consts.smenunotfound=#207#238#228'-'#236#229#237#254' '#237#229' '#237#224+
#233#228#229#237#238

# hash value = 244889058
consts.snotimers=#205#229' '#245#226#224#242#224#229#242' '#228#238#241#242+
#243#239#237#251#245' '#242#224#233#236#229#240#238#226

# hash value = 206529240
consts.snotprinting=#207#240#232#237#242#229#240' '#241#229#233#247#224#241+
' '#237#229' '#237#224#245#238#228#232#242#241#255' '#226' '#240#229#230#232+
#236#229' '#239#229#247#224#242#232

# hash value = 252991836
consts.sprinting=#200#228#184#242' '#239#229#247#224#242#252

# hash value = 90475638
consts.sprinterindexerror=#200#237#228#229#234#241' '#239#240#232#237#242+
#229#240#224' '#226#237#229' '#228#238#239#243#241#242#232#236#251#245' '#227+
#240#224#237#232#246

# hash value = 4471056
consts.sinvalidprinter=#194#238#231#236#238#230#237#238' '#226#251#228#229+
#235#229#237#237#251#233' '#239#240#232#237#242#229#240' '#237#229' '#229+
#241#242#252' '#239#240#232#237#242#229#240

# hash value = 100343811
consts.sdeviceonport='%s '#237#224' %s'


# hash value = 196924360
consts.sgroupindextoolow='GroupIndex '#237#229' '#236#238#230#229#242' '#225+
#251#242#252' '#236#229#237#252#248#229' '#247#229#236' '#239#240#229#228+
#251#228#243#249#232#233' '#238#225#250#229#234#242' '#236#229#237#254' G'+
'roupIndex'


# hash value = 213273792
consts.stwomdiforms=#194' '#238#228#237#238#236' '#239#240#232#235#238#230+
#229#237#232#232' '#237#229' '#236#238#230#229#242' '#225#251#242#252' '#225+
#238#235#229#229' '#247#229#236' '#238#228#237#224' MDI '#244#238#240#236+
#224

# hash value = 75962560
consts.snomdiform=#205#229' '#236#238#227#243' '#241#238#231#228#224#242#252+
' '#244#238#240#236#243'. '#205#232' '#238#228#237#224' '#232#231' MDI '#244+
#238#240#236' '#237#229' '#224#234#242#232#226#232#240#238#226#224#237#224+


# hash value = 76939280
consts.simagecanvasneedsbitmap=#200#231#238#225#240#224#230#229#237#232#229+
' '#236#238#230#237#238' '#232#231#236#229#237#232#242#252' '#242#238#235+
#252#234#238' '#229#241#235#232' '#238#237#238' '#255#226#235#255#229#242+
#241#255' '#240#224#241#242#240#238#226#251#236'(Bitmap) '


# hash value = 167191573
consts.scontrolparentsettoself=#206#225#250#229#234#242' '#237#229' '#236+
#238#230#229#242' '#225#251#242#252' '#240#238#228#232#242#229#235#229#236+
' '#241#224#236' '#241#229#225#229

# hash value = 1339
consts.sokbutton='OK'


# hash value = 232895664
consts.scancelbutton=#206#242#236#229#237#224

# hash value = 13088
consts.syesbutton='&'#196#224

# hash value = 212034
consts.snobutton='&'#205#229#242

# hash value = 65912508
consts.shelpbutton='&'#207#238#236#238#249#252

# hash value = 100272684
consts.sclosebutton='&'#199#224#234#240#251#242#252

# hash value = 78627836
consts.signorebutton='&'#200#227#237#238#240#232#240#238#226#224#242#252

# hash value = 65872352
consts.sretrybutton='&'#207#238#226#242#238#240

# hash value = 165935452
consts.sabortbutton=#207#240#229#234#240#224#242#232#242#252

# hash value = 209397
consts.sallbutton='&'#194#241#229

# hash value = 62824675
consts.scannotdragform=#205#229' '#236#238#227#243' '#239#229#240#229#236+
#229#249#224#242#252' '#244#238#240#236#243

# hash value = 180351459
consts.sputobjecterror=#207#229#240#229#237#229#241#232#242#229' '#238#225+
#250#229#234#242' '#234' '#237#229#243#241#242#224#237#238#226#235#229#237+
#237#238#236#243' '#239#240#229#228#236#229#242#243

# hash value = 149245804
consts.scarddllnotloaded=#205#229' '#236#238#227#243' '#231#224#227#240#243+
#231#232#242#252' CARDS.DLL'


# hash value = 10052448
consts.sduplicatecardid=#205#224#233#228#229#237#238' '#228#226#238#233#237+
#238#229' CardId '


# hash value = 31939113
consts.sddeerr=#206#248#232#225#234#224' '#238#242' DDE  ($0%x)'


# hash value = 84876473
consts.sddeconverr='DDE '#238#248#232#225#234#224' - '#239#229#240#229#227+
#238#226#238#240#251' '#237#229' '#243#241#242#224#237#238#226#235#229#237+
#251' ($0%x)'


# hash value = 55734169
consts.sddememerr=#206#248#232#225#234#224' '#226#251#231#226#224#237#237+
#224#255' '#234#238#227#228#224' '#238#239#229#240#224#242#232#226#237#224+
#255' '#239#224#236#255#242#252' DDE '#239#240#229#226#251#241#232#235#224+
' '#228#238#239#243#241#242#232#236#243#254' ($0%x)'


# hash value = 234149192
consts.sddenoconnect=#205#229#226#238#231#236#238#230#237#238' '#241#226#255+
#231#224#242#252#241#255' '#241' DDE '#239#229#240#229#227#238#226#238#240+
#224#236#232

# hash value = 1186
consts.sfb='FB'


# hash value = 1191
consts.sfg='FG'


# hash value = 1127
consts.sbg='BG'


# hash value = 16258693
consts.soldtshape=#205#229#226#238#231#236#238#230#237#238' '#231#224#227+
#240#243#231#232#242#252' '#240#224#237#237#254#254' '#226#229#240#241#232+
#254' TShape'


# hash value = 16928251
consts.svmetafiles=#204#229#242#224#244#224#233#235#251

# hash value = 118380867
consts.svenhmetafiles='Enhanced Metafiles'


# hash value = 226090120
consts.svicons=#200#234#238#237#234#232

# hash value = 14614800
consts.svbitmaps=#208#224#241#242#240

# hash value = 22053001
consts.sgridtoolarge=#210#224#225#235#232#246#224' '#241#235#232#248#234#238+
#236' '#225#238#235#252#248#224#255' '#228#235#255' '#238#239#229#240#224+
#246#232#233

# hash value = 145752978
consts.stoomanydeleted=#211#228#224#235#229#237#238' '#241#235#232#248#234+
#238#236' '#236#237#238#227#238' '#234#238#235#238#237#238#234' '#232#235+
#232' '#241#242#238#235#225#246#238#226

# hash value = 23982230
consts.sindexoutofrange=#200#237#228#229#234#241' '#242#224#225#235#232#246+
#251' '#226#237#229' '#228#238#225#243#241#242#232#236#251#245' '#227#240+
#224#237#232#246

# hash value = 124002522
consts.sfixedcoltoobig=#202#238#235#232#247#229#241#242#226#238' '#244#232+
#234#241#232#240#238#226#224#237#237#251#245' '#234#238#235#238#237#238#234+
' '#228#238#235#230#237#238' '#225#251#242#252' '#236#229#237#252#248#229+
', '#247#229#236' '#241#224#236#232#245' '#234#238#235#238#237#238#234

# hash value = 71378642
consts.sfixedrowtoobig=#202#238#235#232#247#229#241#242#226#238' '#244#232+
#234#241#232#240#238#226#224#237#237#251#245' '#241#242#238#235#225#246#238+
#226' '#228#238#235#230#237#238' '#225#251#242#252' '#236#229#237#252#248+
#229', '#247#229#236' '#241#224#236#232#245' '#241#242#238#235#225#246#238+
#226

# hash value = 129237531
consts.sinvalidstringgridop=#205#229#226#238#231#236#238#230#237#238' '#226+
#241#242#224#226#232#242#252' '#232#235#232' '#243#228#224#235#232#242#252+
' '#241#242#238#225#229#246' '#232#231' '#242#224#225#235#232#246#251

# hash value = 72088512
consts.sinvalidenumvalue=#205#229#226#229#240#237#224#255' Enum '#226#229+
#235#232#247#232#237#224

# hash value = 218697359
consts.sinvalidnumber=#205#229#226#229#240#237#224#255' '#237#243#236#229+
#240#224#246#232#255

# hash value = 173478557
consts.soutlineindexerror=#200#237#228#229#234#241' '#226#237#229' '#235#232+
#237#232#233' '#237#229' '#237#224#233#228#229#237

# hash value = 32936381
consts.soutlineexpanderror=#208#238#228#232#242#229#235#252' '#228#238#235+
#230#229#237' '#225#251#242#252' '#240#224#241#248#232#240#229#237

# hash value = 40122576
consts.sinvalidcurrentitem=#205#229#226#229#240#237#224#255' '#226#229#235+
#232#247#232#237#224' '#228#235#255' '#228#224#237#237#238#227#238' '#238+
#225#250#229#234#242#224

# hash value = 92813936
consts.smaskerr=#205#229#226#229#240#237#224#255' '#226#226#238#228#237#224+
#255' '#226#229#235#232#247#232#237#224

# hash value = 219825311
consts.smaskediterr=#205#229#226#229#240#237#224#255' '#226#226#238#228#237+
#224#255' '#226#229#235#232#247#232#237#224'. '#200#241#239#238#235#252#231+
#243#233#242#229' Escape, '#247#242#238#225#251' '#238#242#236#229#237#232+
#242#252' '#232#231#236#229#237#229#237#232#255

# hash value = 220709064
consts.soutlineerror='Invalid outline index'


# hash value = 200636607
consts.soutlinebadlevel=#205#229#226#229#240#237#238#229' '#239#240#232#241+
#226#238#229#237#232#229' '#243#240#238#226#237#255

# hash value = 105795093
consts.soutlineselection=#205#229#226#229#240#237#238#229' '#226#251#228#229+
#235#229#237#232#229

# hash value = 15344432
consts.soutlinefileload=#206#248#232#225#234#224' '#231#224#227#240#243#231+
#234#232' '#244#224#233#235#224

# hash value = 259875807
consts.soutlinelongline=#203#232#237#232#255' '#241#235#232#248#234#238#236+
' '#228#235#232#237#237#224#255

# hash value = 140863696
consts.soutlinemaxlevels=#196#238#241#242#232#227#237#243#242#224' '#236#224+
#234#241#232#236#224#235#252#237#224#255' '#227#235#243#225#232#237#224

# hash value = 209385333
consts.smsgdlgwarning=#194#237#232#236#224#237#232#229

# hash value = 233271424
consts.smsgdlgerror=#206#248#232#225#234#224

# hash value = 234330703
consts.smsgdlginformation=#200#237#244#238#240#236#224#246#232#255

# hash value = 27737077
consts.smsgdlgconfirm=#207#238#228#242#226#229#240#230#228#229#237#232#229+


# hash value = 13088
consts.smsgdlgyes='&'#196#224

# hash value = 212034
consts.smsgdlgno='&'#205#229#242

# hash value = 1339
consts.smsgdlgok='OK'


# hash value = 232895664
consts.smsgdlgcancel=#206#242#236#229#237#224

# hash value = 65912508
consts.smsgdlghelp='&'#207#238#236#238#249#252

# hash value = 104953184
consts.smsgdlghelpnone=#207#238#236#238#249#252' '#237#229#228#238#241#242+
#243#239#237#224

# hash value = 233684620
consts.smsgdlghelphelp=#207#238#236#238#249#252

# hash value = 28612092
consts.smsgdlgabort='&'#206#242#236#229#237#232#242#252

# hash value = 65872352
consts.smsgdlgretry='&'#207#238#226#242#238#240

# hash value = 78627836
consts.smsgdlgignore='&'#200#227#237#238#240#232#240#238#226#224#242#252

# hash value = 209397
consts.smsgdlgall='&'#194#241#229

# hash value = 240189164
consts.smsgdlgnotoall=#205#229' '#239#240#232#236#229#237#255#242#252' '#234+
#238' '#226#241#229#236

# hash value = 225834844
consts.smsgdlgyestoall=#207#240#232#236#229#237#232#242#252' '#234#238' '#226+
#241#229#236

# hash value = 299168
consts.smkcbksp='BkSp'


# hash value = 23154
consts.smkctab='Tab'


# hash value = 19603
consts.smkcesc='Esc'


# hash value = 5003970
consts.smkcenter='Enter'


# hash value = 5924757
consts.smkcspace='Space'


# hash value = 355520
consts.smkcpgup='PgUp'


# hash value = 355246
consts.smkcpgdn='PgDn'


# hash value = 19524
consts.smkcend='End'


# hash value = 325173
consts.smkchome='Home'


# hash value = 338900
consts.smkcleft='Left'


# hash value = 1472
consts.smkcup='Up'


# hash value = 5832180
consts.smkcright='Right'


# hash value = 308958
consts.smkcdown='Down'


# hash value = 20563
consts.smkcins='Ins'


# hash value = 19132
consts.smkcdel='Del'


# hash value = 94305643
consts.smkcshift='Shift+'


# hash value = 4897003
consts.smkcctrl='Ctrl+'


# hash value = 295787
consts.smkcalt='Alt+'


# hash value = 86327753
consts.srunknown='(Unknown)'


# hash value = 47539321
consts.srnone='(None)'


# hash value = 90469428
consts.soutofrange=#194#229#235#232#247#232#237#224' '#228#238#235#230#237+
#224' '#225#251#242#252' '#236#229#230#228#243' %d '#232' %d'


# hash value = 173324091
consts.sdateencodeerror=#205#229#226#229#240#237#251#233' '#224#240#227#243+
#236#229#237#242' '#228#235#255' '#228#229#234#238#228#232#240#238#226#234+
#232' '#228#224#242#251

# hash value = 128304538
consts.sdefaultfilter=#194#241#229' '#244#224#233#235#251' (*.*)|*.*'


# hash value = 53749
consts.sallfilter=#194#241#229

# hash value = 197363325
consts.snovolumelabel=': [ - no volume label - ]'


# hash value = 219190734
consts.sinsertlineerror=#205#229#226#238#231#236#238#230#237#238' '#226#241+
#242#224#226#232#242#252' '#235#232#237#232#254

# hash value = 40249583
consts.sconfirmcreatedir=#211#241#242#224#237#238#226#235#229#237#237#224+
#255' '#228#232#240#229#234#242#238#240#232#255' '#237#229' '#241#243#249+
#229#241#242#226#243#229#242'. '#209#238#231#228#224#242#252'?'


# hash value = 39888670
consts.sselectdircap=#194#251#225#229#240#232#242#229' '#228#232#240#229#234+
#242#238#240#232#254

# hash value = 168803770
consts.sdirnamecap='&'#200#236#255' '#228#232#240#229#234#242#238#240#232+
#232':'


# hash value = 45484138
consts.sdrivescap=#196#232'&'#241#234#232':'


# hash value = 189704698
consts.sdirscap='&'#196#232#240#229#234#242#238#240#232#232':'


# hash value = 134568473
consts.sfilescap='&'#212#224#233#235#251': (*.*)'


# hash value = 140639998
consts.snetworkcap=#209#229'&'#242#252'...'


# hash value = 4875106
consts.scolorprefix='Color'


# hash value = 109054432
consts.scolortags='ABCDEFGHIJKLMNOP'


# hash value = 172024912
consts.sinvalidclipfmt=#205#229#226#229#240#237#251#233' '#244#238#240#236+
#224#242' '#225#243#244#229#240#224

# hash value = 112487849
consts.sicontoclipboard=#193#243#244#229#240' '#237#229' '#239#238#228#228+
#229#240#230#232#226#224#229#242' '#232#231#238#225#240#224#230#229#237#232+
#229' '#232#234#238#237#238#234' (Icons)'


# hash value = 148474816
consts.scannotopenclipboard=#205#229#226#238#231#236#238#230#237#238' '#238+
#242#234#240#251#242#252' '#225#243#244#229#240

# hash value = 39123662
consts.sdefault=#207#238' '#243#236#238#235#247#224#237#232#254

# hash value = 243710287
consts.sinvalidmemosize=#210#229#234#241#242' '#239#240#229#226#251#241#232+
#235' '#229#236#234#238#241#242#252' '#234#238#236#239#238#237#229#237#242+
#224' Memo'


# hash value = 184580672
consts.scustomcolors=#206#225#251#247#237#251#229' '#246#226#229#242#224

# hash value = 188135539
consts.sinvalidprinterop=#205#229' '#239#238#228#228#229#240#230#232#226#224+
#229#236#224#255' '#238#239#229#240#224#246#232#255' '#234' '#226#251#228+
#229#235#229#237#237#238#236#243' '#239#240#232#237#242#229#240#243

# hash value = 112620688
consts.snodefaultprinter=#199#228#229#241#252' '#237#229' '#226#251#225#240+
#224#237' '#239#240#232#237#242#229#240

# hash value = 259051299
consts.sinifilewriteerror=#205#229#226#238#231#236#238#230#237#224' '#231+
#224#239#232#241#252' '#234' %s'


# hash value = 248393430
consts.sbitsindexerror=#200#237#228#229#234#241' '#225#232#242#238#226' '#226+
#237#229' '#228#238#239#243#241#242#232#236#251#245' '#227#240#224#237#232+
#246

# hash value = 185202489
consts.suntitled='(Untitled)'


# hash value = 107685959
consts.sinvalidregtype=#205#229#226#229#240#237#251#233' '#242#232#239' '#228+
#224#237#237#251#245' '#228#235#255' '#39'%s'#39

# hash value = 108960441
consts.sunknownconversion=#205#229#232#231#226#229#241#242#237#251#233' '#244+
#224#233#235' RichEdit (.%s)'


# hash value = 255872489
consts.sduplicatemenus=#204#229#237#254' '#39'%s'#39' '#243#230#229' '#232+
#241#239#238#235#252#231#243#229#242#241#255' '#228#240#243#227#238#233' '+
#244#238#240#236#238#233

# hash value = 71726698
consts.spicturelabel=#200#231#238#225#240#224#230#229#237#232#229':'


# hash value = 180070729
consts.spicturedesc=' (%dx%d)'


# hash value = 78633333
consts.spreviewlabel=#207#240#229#228#251#228#243#249#229#229

# hash value = 143159033
consts.scannotopenavi=#205#229#226#238#231#236#238#230#237#238' '#238#242+
#234#240#251#242#252' AVI'


# hash value = 135147042
consts.snotopenerr=#205#229#242' '#238#242#234#240#251#242#251#245' MCI '#236+
#229#245#224#237#232#231#236#238#226

# hash value = 12308057
consts.smpopenfilter=#194#241#229' '#244#224#233#235#251' (*.*)|*.*|Wave '+
#244#224#233#235#251' (*.wav)|*.wav|Midi '#244#224#233#235#251' (*.mid)|*'+
'.mid|'#194#232#228#229#238' '#228#235#255' Windows (*.avi)|*.avi'


# hash value = 4294967295
consts.smcinil=

# hash value = 183303903
consts.smciavivideo='AVIVideo'


# hash value = 126401215
consts.smcicdaudio='CDAudio'


# hash value = 18532
consts.smcidat='DAT'


# hash value = 78846495
consts.smcidigitalvideo='DigitalVideo'


# hash value = 35941541
consts.smcimmmovie='MMMovie'


# hash value = 222307013
consts.smciother=#196#240#243#227#238#229

# hash value = 237823710
consts.smcioverlay=#192#237#224#235#238#227#238#226#238#229' '#243#241#242+
#240#238#233#241#242#226#238

# hash value = 235469888
consts.smciscanner=#209#234#224#237#229#240

# hash value = 147685547
consts.smcisequencer='MIDI-'#244#224#233#235

# hash value = 100127184
consts.smcivcr=#194#232#228#229#238#234#224#241#241#229#242#224

# hash value = 87480458
consts.smcivideodisc=#194#232#228#229'-'#234#238#236#239#224#234#242'-'#228+
#232#241#234

# hash value = 144518315
consts.smciwaveaudio='Wav-'#244#224#233#235

# hash value = 85218944
consts.smciunknownerror=#205#229#233#231#226#229#241#242#237#224#255' '#238+
#248#232#225#234#224

# hash value = 88285331
consts.sbolditalicfont='Bold Italic'


# hash value = 300580
consts.sboldfont='Bold'


# hash value = 84574963
consts.sitalicfont='Italic'


# hash value = 146719442
consts.sregularfont='Regular'


# hash value = 31006478
consts.spropertiesverb=#209#226#238#233#241#242#226#238

# hash value = 211455443
consts.sservicefailed='Service failed on %s: %s'


# hash value = 216771781
consts.sexecute='execute'


# hash value = 8038548
consts.sstart='start'


# hash value = 502624
consts.sstop='stop'


# hash value = 7769237
consts.spause='pause'


# hash value = 106627861
consts.scontinue='continue'


# hash value = 168133221
consts.sinterrogate='interrogate'


# hash value = 264941902
consts.sshutdown='shutdown'


# hash value = 29144579
consts.scustomerror='Service failed in custom message(%d): %s'


# hash value = 195005129
consts.sserviceinstallok='Service installed successfully'


# hash value = 59960802
consts.sserviceinstallfailed='Service "%s" failed to install with error: '+
'"%s"'


# hash value = 208480985
consts.sserviceuninstallok='Service uninstalled successfully'


# hash value = 134128226
consts.sserviceuninstallfailed='Service "%s" failed to uninstall with err'+
'or: "%s"'


# hash value = 120286414
consts.sinvalidactionregistration='Invalid action registration'


# hash value = 107435102
consts.sinvalidactionunregistration='Invalid action unregistration'


# hash value = 257061422
consts.sinvalidactionenumeration='Invalid action enumeration'


# hash value = 14455774
consts.sinvalidactioncreation='Invalid action creation'


# hash value = 140560469
consts.sdockedctlneedsname='Docked control must have a name'


# hash value = 229678005
consts.sdocktreeremoveerror='Error removing control from dock tree'


# hash value = 158451316
consts.sdockzonenotfound=' - Dock zone not found'


# hash value = 101523788
consts.sdockzonehasnoctl=' - Dock zone has no control'


# hash value = 46243859
consts.sallcommands='All Commands'


# hash value = 83690201
consts.sduplicateitem='List does not allow duplicates ($0%x)'


# hash value = 91829570
consts.stextnotfound=#210#229#234#241#242' '#237#229' '#237#224#233#228#229+
#237': "%s"'


# hash value = 172090516
consts.sbrowserexecerror='No default browser is specified'


# hash value = 179686350
consts.scolorboxcustomcaption='Custom...'


# hash value = 240242773
consts.smultiselectrequired='Multiselect mode must be on for this feature'+


# hash value = 891863
consts.skeycaption=#202#235#254#247

# hash value = 78146720
consts.svaluecaption=#194#229#235#232#247#232#237#224

# hash value = 220319714
consts.skeyconflict=#202#235#254#247' '#241' '#232#236#229#237#229#236' "'+
'%s" '#211#230#229' '#241#243#249#229#241#242#226#243#229#242

# hash value = 207162589
consts.skeynotfound=#202#235#254#247' "%s" '#237#229' '#237#224#233#228#229+
#237

# hash value = 10016558
consts.snocolumnmoving='goColMoving is not a supported option'


# hash value = 129300921
consts.snoequalsinkey='Key may not contain equals sign ("=")'


# hash value = 256258703
consts.ssenderror=#206#248#232#225#234#224' '#238#242#239#240#224#226#234+
#232' '#241#238#238#225#249#229#237#232#255

# hash value = 221963954
consts.sassignsubitemerror='Cannot assign a subitem to an actionbar when '+
'one of it'#39's parent'#39's is already assigned to an actionbar'


# hash value = 182353631
consts.sdeleteitemwithsubitems='Item %s has subitems, delete anyway?'


# hash value = 42328765
consts.sdeletenotallowed='You are not allowed to delete this item'


# hash value = 262941780
consts.smovenotallowed='Item %s is not allowed to be moved'


# hash value = 258022506
consts.smorebuttons=#193#238#235#252#248#229' '#234#237#238#239#238#234

# hash value = 43672243
consts.serrordownloadingurl=#206#248#232#225#234#224' '#231#224#227#240#243+
#231#234#232' URL: %s'


# hash value = 228710163
consts.surlmondllmissing=#205#229#226#238#231#236#238#230#237#238' '#231#224+
#227#240#243#231#232#242#252' %s'


# hash value = 103727369
consts.sallactions='(All Actions)'


# hash value = 132980761
consts.snocategory='(No Category)'


# hash value = 80701508
consts.sexpand='Expand'


# hash value = 191398434
consts.serrorsettingpath=#206#248#232#225#234#224' '#243#241#242#224#237#238+
#226#234#232' '#239#243#242#232': "%s"'


# hash value = 241652904
consts.slbputerror='Attempting to put items into a virtual style listbox'


# hash value = 43406639
consts.serrorloadingfile='Error loading previously saved settings file: %'+
's'#13'Would you like to delete it?'


# hash value = 13655423
consts.sresetusagedata='Reset all usage data?'


# hash value = 22974
consts.sfilerundialogtitle='Run'


# hash value = 22310441
consts.snoname='(No Name)'


# hash value = 26288260
consts.serroractionmanagernotassigned='ActionManager must first be assign'+
'ed'


# hash value = 127504691
consts.saddremovebuttons='&Add or Remove Buttons'


# hash value = 61730434
consts.sresetactiontoolbar='Reset Toolbar'


# hash value = 179874757
consts.scustomize='&Customize'


# hash value = 109630626
consts.sseparator='Separator'


# hash value = 228681140
consts.scirularreferencesnotallowed='Circular references not allowed'


# hash value = 236488167
consts.scannothideactionband='%s does not allow hiding'


# hash value = 183479732
consts.serrorsettingcount='Error setting %s.Count'


# hash value = 109334388
consts.slistboxmustbevirtual='Listbox (%s) style must be virtual in order'+
' to set Count'


# hash value = 119375251
consts.sunabletosavesettings='Unable to save settings'


# hash value = 64834287
consts.srestoredefaultschedule='Would you like to reset to the default Pr'+
'iority Schedule?'


# hash value = 229256692
consts.snogetitemeventhandler='No OnGetItem event handler assigned'


# hash value = 16397464
consts.sinvalidcolormap='Invalid Colormap this ActionBand requires ColorM'+
'aps of type TCustomActionBarColorMapEx'


# hash value = 81436509
consts.sduplicateactionbarstylename=#209#242#232#235#252' '#241' '#232#236+
#229#237#229#236' %s '#243#230#229' '#231#224#240#229#227#232#241#242#240+
#232#240#238#226#224#237

# hash value = 167781372
consts.sstandardstyleactionbars=#209#242#224#237#228#224#240#242#237#251#233+
' '#241#242#232#235#252

# hash value = 48305276
consts.sxpstyleactionbars='XP '#209#242#232#235#252

# hash value = 58887358
consts.sactionbarstylemissing='No ActionBand style unit present in the us'+
'es clause.'#13'Your application must include either XPStyleActnCtrls, St'+
'dStyleActnCtrls or a third party ActionBand style unit in its uses claus'+
'e.'

