unit Unit2;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, ComCtrls, LCLProc;

type

  { TReg }

  TReg = class(TForm)
    ComboBox1: TComboBox;
    LoginIn_button: TButton;
    first_nameedit: TEdit;
    last_nameedit: TEdit;
    middle_nameedit: TEdit;
    first_nameText: TLabel;
    Last_nameText: TLabel;
    middle_nameText: TLabel;
    middle_nameText1: TLabel;
    passno: TImage;
    pass2yes: TImage;
    passyes: TImage;
    pass2no: TImage;
    loginedit: TEdit;
    MailText: TLabel;
    passedit: TEdit;
    pass2edit: TEdit;
    mailedit: TEdit;
    loginText: TLabel;
    PassText: TLabel;
    Pass2Text: TLabel;
    Label5: TLabel;
    StatusBar1: TStatusBar;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure LoginIn_buttonClick(Sender: TObject);
    procedure pass2editChange(Sender: TObject);
    procedure passeditChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Reg: TReg;
  password_OK,confirm_password_OK,login_OK,mail_OK: boolean;

implementation
uses Unit1;

{$R *.lfm}

{ TReg }

procedure TReg.LoginIn_buttonClick(Sender: TObject);
var password,first_name,last_name,middle_name,ip_global_reg,ip_local_reg,computer_name_reg,err: string;
begin
  if (loginedit.text<>'') and (passedit.text<>'') and (pass2edit.text<>'') and
  (mailedit.text<>'') and (first_nameedit.text<>'') and (last_nameedit.text<>'') and
  (middle_nameedit.text<>'') and (ComboBox1.itemindex<>0) then
  begin
    login:=loginedit.text;
    password:=pass_gen(passedit.text);
    mail:=mailedit.text;
    first_name:=first_nameedit.text;
    last_name:=last_nameedit.text;
    middle_name:=middle_nameedit.text;
    ip_global_reg:=GetIP;
    GetIPFromHost(computer_name_reg, ip_local_reg, err);
    try
      Auth.MySQL56Connection1.Connected:=true;
    except
      messagedlg('Ошибка подключения к БД!',mterror,[mbok],0);
      exit;
    end;
    try
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
      Auth.SQLQuery1.ExecSQL;
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('SELECT login FROM users WHERE login=:login');
      Auth.SQLQuery1.Params.ParamByName('login').value:=login;
      try
        Auth.SQLQuery1.Open;
      except
        Auth.SQLQuery1.SQL.Clear;
        Auth.MySQL56Connection1.Connected:=false;
        messagedlg('Ошибка получения таблицы!',mterror,[mbok],0);
        exit;
      end;
      if Auth.SQLQuery1.recordcount>0 then
      begin
        Auth.SQLQuery1.SQL.Clear;
        Auth.MySQL56Connection1.Connected:=false;
        messagedlg('Такой логин уже занят!',mterror,[mbok],0);
        exit;
      end
      else
      begin
        Auth.SQLQuery1.SQL.Clear;
        Auth.SQLQuery1.SQL.add('INSERT INTO users (login,password,mail,last_name,first_name,middle_name,ip_global_reg,ip_local_reg,computer_name_reg,mac_reg,OS,user_group) VALUES(:login,:password,:mail,:last_name,:first_name,:middle_name,:ip_global_reg,:ip_local_reg,:computer_name_reg,:mac_reg,:OS,:user_group)');
        Auth.SQLQuery1.Params.ParamByName('login').value:=login;
        Auth.SQLQuery1.Params.ParamByName('password').value:=password;
        Auth.SQLQuery1.Params.ParamByName('mail').value:=mail;
        Auth.SQLQuery1.Params.ParamByName('last_name').value:=last_name;
        Auth.SQLQuery1.Params.ParamByName('first_name').value:=first_name;
        Auth.SQLQuery1.Params.ParamByName('middle_name').value:=middle_name;
        Auth.SQLQuery1.Params.ParamByName('ip_global_reg').value:=ip_global_reg;
        Auth.SQLQuery1.Params.ParamByName('ip_local_reg').value:=ip_local_reg;
        Auth.SQLQuery1.Params.ParamByName('computer_name_reg').value:=computer_name_reg;
        Auth.SQLQuery1.Params.ParamByName('mac_reg').value:=TAuth.GetMacAddress();
        Auth.SQLQuery1.Params.ParamByName('OS').value:=OSVersion;
        Auth.SQLQuery1.Params.ParamByName('user_group').value:=ComboBox1.items[ComboBox1.itemindex];
        Auth.SQLQuery1.ExecSQL;
        Auth.SQLQuery1.SQL.Clear;
        Auth.MySQL56Connection1.Connected:=false;
        messagedlg('Вы успешно зарегистрированы, авторизируйтесь!',mtCustom,[mbok],0);
        Auth.Show;
        Reg.Release;
      end;
    except
      Auth.SQLQuery1.SQL.Clear;
      Auth.MySQL56Connection1.Connected:=false;
      messagedlg('Ошибка отправки запроса!',mterror,[mbok],0);
      exit;
    end;
  end
  else
  begin
    messagedlg('Вы должны заполнить все поля!',mterror,[mbok],0);
    exit;
  end;
end;

procedure TReg.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  Auth.show;
end;

procedure TReg.pass2editChange(Sender: TObject);
begin
  if (passedit.text=pass2edit.text) and (UTF8Length(passedit.text) >=4) and
  (UTF8Length(passedit.text)<=64) then
  begin
    pass2yes.visible:=true;
    pass2no.visible:=false;
    confirm_password_OK:=true;
  end
  else
  begin
    pass2yes.visible:=false;
    pass2no.visible:=true;
    confirm_password_OK:=false;
  end;
  if (password_OK=false) and (confirm_password_OK=false) then
  statusbar1.simpletext:='Пароль меньше 4 или больше 64 символов.'
  else if (password_OK=true) and (confirm_password_OK=false) then
  statusbar1.simpletext:='Пароли не совпадают.'
  else if (password_OK=false) and (confirm_password_OK=true) then
  statusbar1.simpletext:='Пароль меньше 4 или больше 64 символов.'
  else if (password_OK=true) and (confirm_password_OK=true) then
  statusbar1.simpletext:='';
end;

procedure TReg.passeditChange(Sender: TObject);
begin
  if (UTF8Length(passedit.text)>=4) and
  (UTF8Length(passedit.text)<=64) then
  begin
    passyes.visible:=true;
    passno.visible:=false;
    password_OK:=true;
  end
  else
  begin
    passyes.visible:=false;
    passno.visible:=true;
    password_OK:=false;
  end;
  if (passedit.text=pass2edit.text) and (UTF8Length(passedit.text)>=4) and
  (UTF8Length(passedit.text)<=64) then
  begin
    pass2yes.visible:=true;
    pass2no.visible:=false;
    confirm_password_OK:=true;
  end
  else
  begin
    pass2yes.visible:=false;
    pass2no.visible:=true;
    confirm_password_OK:=false;
  end;
  if (password_OK=false) and (confirm_password_OK=false) then
  statusbar1.simpletext:='Пароль меньше 4 или больше 64 символов.'
  else if (password_OK=true) and (confirm_password_OK=false) then
  statusbar1.simpletext:='Пароли не совпадают.'
  else if (password_OK=false) and (confirm_password_OK=true) then
  statusbar1.simpletext:='Пароль меньше 4 или больше 64 символов.'
  else if (password_OK=true) and (confirm_password_OK=true) then
  statusbar1.simpletext:='';
end;

end.

