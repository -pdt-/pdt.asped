unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, mysql56conn, sqldb, db, FileUtil, Forms, Controls,
  Graphics, Dialogs, StdCtrls, ExtCtrls, ZConnection, ZDataset, md5, HTTPSEND,
  Winsock, win32proc, resource, versiontypes, versionresource,
  ActiveX, ComObj, Variants, LazUTF8, LCLProc;

type

  { TAuth }

  TAuth = class(TForm)
    DataSource2: TDataSource;
    MyQuery: TZQuery;
    MySQL56Connection2: TZConnection;
    Reg_button: TLabel;
    LoginIn: TButton;
    DataSource1: TDataSource;
    Loginedit: TEdit;
    Passedit: TEdit;
    Login_label: TLabel;
    Password_label: TLabel;
    Timer: TTimer;
    MySQL56Connection1: TZConnection;
    SQLQuery1: TZQuery;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure MySQL56Connection1AfterConnect(Sender: TObject);
    procedure Reg_buttonClick(Sender: TObject);
    procedure LoginInClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    class function GetMacAddress : string;
  end;

type
  TMyThread = class(TThread)
  private
    FQuery: TZQuery;
  protected
    procedure Execute; override;
  public
    constructor Create(AQuery: TZQuery);
  end;

type
  TUpdater = class(TThread)
  private
    QQ: TZQuery;
  protected
    procedure Execute; override;
  public
    constructor Create(MQuery: TZQuery);
  end;
  function GetIPFromHost(var HostName, IPaddr, WSAErr: string): boolean;
  function pass_gen(password: string): string;
  function GetIP: string;
  function OSVersion: string;
  function resourceVersionInfo: string;
  procedure MainMenu_reload;
  function CheckUser: boolean;
  function DelLastEnter(s: string): string;
  function GetSerialNumber(var ASerial: WideString): Boolean;
  procedure bd_log;
  {$IFDEF WINDOWS}
  function GetIfTable( pIfTable : Pointer;
                   VAR pdwSize  : LongInt;
                       bOrder   : LongInt ): LongInt; stdcall;
  {$ENDIF}

var
  Auth: TAuth;
  MyThread: TMyThread;
  Updater: TUpdater;
  login,mail,FIO,user_group,update_URL,user_flags: string;
  serial: WideString;
  user_id,variant_user: integer;
  resume_test: array of array of array of integer;
  const error_login_count: integer = 0;
  time_login_count: integer = 10;

implementation
uses Unit2, Unit3, Unit5, Unit9, Unit12;
{$IFDEF WINDOWS}
function GetIfTable( pIfTable : Pointer;
                 VAR pdwSize  : LongInt;
                     bOrder   : LongInt ): LongInt; stdcall; external 'IPHLPAPI.DLL';
{$ENDIF}

//==================== Поток 1 =================
constructor TMyThread.Create(AQuery: TZQuery);
begin
  inherited Create(true);
  FreeOnTerminate:=false;
  FQuery:=AQuery;
end;

procedure TMyThread.Execute;
begin
  Auth.MySQL56Connection2.Connected:=true;
  synchronize(@FQuery.ExecSQL);
  Auth.MySQL56Connection2.Connected:=false;
  FQuery.SQL.Clear;
  MyThread.Terminate;
  MyThread.Free;
end;
//==============================================


//==================== Поток 2 =================
constructor TUpdater.Create(MQuery: TZQuery);
begin
  inherited Create(true);
  FreeOnTerminate:=false;
  QQ:=MQuery;
end;

procedure TUpdater.Execute;
var current_version: string;
begin
  current_version:=resourceVersionInfo;
  Auth.MySQL56Connection2.Connected:=true;
  QQ.SQL.Clear;
  QQ.SQL.add('SELECT * FROM program_info WHERE version>:current_version ORDER BY version DESC');
  QQ.Params.ParamByName('current_version').value:=current_version;
  QQ.Open;
  if QQ.RecordCount=1 then
  begin
    update_URL:=QQ.FieldByName('url_download').value;
    Application.CreateForm(TUpdate_form, Update_form);
    synchronize(@Update_form.show);
    synchronize(@auth.Hide);
    Update_form.RichMemo1.clear;
    update_form.label2.caption:='Текущая версия: '+current_version;
    Update_form.RichMemo1.lines.Add('Список свежих обновлений:');
    Update_form.RichMemo1.lines.Add('');
    while not QQ.EOF do
    begin
      Update_form.RichMemo1.lines.Add('**********');
      Update_form.RichMemo1.lines.Add(QQ.FieldByName('version').value);
      Update_form.RichMemo1.lines.Add('Изменения:');
      Update_form.RichMemo1.lines.Add(QQ.FieldByName('change_log').value);
      Update_form.RichMemo1.lines.Add('**********');
      Update_form.RichMemo1.lines.Add('');
      QQ.Next;
    end;
    Update_form.RichMemo1.lines.delete(Update_form.RichMemo1.lines.count-1);
    Update_form.RichMemo1.SelLength:=0;
    Update_form.RichMemo1.SelStart:=0;
  end;
  Auth.MySQL56Connection2.Connected:=false;
  QQ.SQL.Clear;
  Updater.Terminate;
  Updater.Free;
end;
//==============================================

{$R *.lfm}

{ TAuth }

function Translate(Name,Value : AnsiString; Hash : Longint; arg:pointer) : AnsiString;
begin
  case StringCase(Value,['&Yes','&No','Cancel']) of
   0: Result:='&Да';
   1: Result:='&Нет';
   2: Result:='Отмена';
   else Result:=Value;
  end;
end;

procedure bd_log;
begin
  MyThread:=TMyThread.Create(Auth.MyQuery);
  MyThread.Start;
end;

procedure upd;
//var current_version: string;
begin
  Updater:=TUpdater.Create(Auth.MyQuery);
  Updater.Start;
end;

function DelLastEnter(s: string):string;
begin
 if s[Length(s)] = #10
  then
   Delete(s, Length(s)-1, 2);
 Result:=s;
end;

procedure MainMenu_reload;
var i,n,g: integer;
  mark: string;
begin
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT login,user_flags,first_name,middle_name,id,user_flags,confirmed,mail,ban_reason,user_group FROM users WHERE id=:user_id');
  Auth.SQLQuery1.Params.ParamByName('user_id').value:=user_id;
  Auth.SQLQuery1.open;
  user_flags:=Auth.SQLQuery1.FieldByName('user_flags').value;
  FIO:=Auth.SQLQuery1.FieldByName('first_name').value+' '+Auth.SQLQuery1.FieldByName('middle_name').value;
  MainMenu_Form.hello_text.caption:='Добро пожаловать, '+FIO;
  MainMenu_Form.label6.caption:='ID: '+Auth.SQLQuery1.FieldByName('id').asstring;
  MainMenu_Form.label7.caption:='Логин: '+Auth.SQLQuery1.FieldByName('login').asstring;
  MainMenu_Form.label8.caption:='Права доступа: '+Auth.SQLQuery1.FieldByName('user_flags').asstring;
  MainMenu_Form.label3.caption:='Подтвержден: '+Auth.SQLQuery1.FieldByName('confirmed').asstring;
  MainMenu_Form.label4.caption:='Почта: '+Auth.SQLQuery1.FieldByName('mail').asstring;
  if (Auth.SQLQuery1.FieldByName('user_flags').value='admin') or
  (Auth.SQLQuery1.FieldByName('user_flags').value='teacher') or
  (Auth.SQLQuery1.FieldByName('user_flags').value='moderator') then
  begin
    mainmenu_form.CP_Button.visible:=true;
    mainmenu_form.CP_Button.enabled:=true;
  end
  else
  begin
    mainmenu_form.CP_Button.visible:=false;
    mainmenu_form.CP_Button.enabled:=false;
    if Auth.SQLQuery1.FieldByName('user_flags').value='banned' then
    begin
      messagedlg('Внимание!','Ваш аккаунт заблокирован по причине'+slinebreak+'"'+
      Auth.SQLQuery1.FieldByName('ban_reason').value+'"',mterror,[mbok],0);
      Auth.MySQL56Connection1.Connected:=false;
      Auth.SQLQuery1.SQL.Clear;
      application.terminate;
      exit;
    end;
  end;
  user_group:=Auth.SQLQuery1.FieldByName('user_group').value;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT id,name,active,variant,CURRENT_TIMESTAMP>active_from,CURRENT_TIMESTAMP>active_to,mark,user_group,id_test,action FROM tests t LEFT JOIN (SELECT id_table,id_test,action,id_log FROM user_test_log ');
  Auth.SQLQuery1.SQL.add('WHERE user_id=:user_id ORDER BY id_log DESC) ul ON t.id=ul.id_table ');
  Auth.SQLQuery1.SQL.add('GROUP BY t.id DESC;');
  Auth.SQLQuery1.Params.ParamByName('user_id').value:=user_id;
  Auth.SQLQuery1.Open;
  MainMenu_Form.Listbox1.enabled:=true;
  MainMenu_form.ListBox1.Items.clear;
  SetLength(resume_test,Auth.SQLQuery1.FieldByName('id').AsInteger,2,5);
  i:=0;
  while not Auth.SQLQuery1.EOF do
  begin
    if (pos('конец теста',Auth.SQLQuery1.FieldByName('action').AsString)=0) and
    (Auth.SQLQuery1.FieldByName('active').AsString='yes') and
    ((pos(inttostr(variant_user),Auth.SQLQuery1.FieldByName('variant').AsString)<>0) or (Auth.SQLQuery1.FieldByName('variant').AsString='')) and
    (Auth.SQLQuery1.FieldByName('CURRENT_TIMESTAMP>active_to').value=0) and
    (Auth.SQLQuery1.FieldByName('CURRENT_TIMESTAMP>active_from').value=1) and
    (pos(user_group,Auth.SQLQuery1.FieldByName('user_group').AsString)<>0) then
    begin
      MainMenu_form.ListBox1.Items.Add(Auth.SQLQuery1.FieldByName('Name').AsString);
      resume_test[i,0,0]:=Auth.SQLQuery1.FieldByName('id').AsInteger;
      if (pos('Принудительное закрытие теста',Auth.SQLQuery1.FieldByName('action').AsString)=1) or
      (pos('Возобновление теста',Auth.SQLQuery1.FieldByName('action').AsString)=1) then
      resume_test[i,1,0]:=Auth.SQLQuery1.FieldByName('id_test').AsInteger
      else
      if pos('запрос следующего',Auth.SQLQuery1.FieldByName('action').AsString)>0 then
      resume_test[i,1,0]:=Auth.SQLQuery1.FieldByName('id_test').AsInteger+1
      else
      resume_test[i,1,0]:=1;
      mark:=Auth.SQLQuery1.FieldByName('mark').AsString;
      for g:=1 to 4 do
      begin
        n:=pos(';',mark);
        resume_test[i,1,g]:=strtoint(copy(mark,3,2));
        delete(mark,1,n);
      end;
      i:=i+1;
    end;
    Auth.SQLQuery1.Next;
  end;
  Auth.SQLQuery1.SQL.Clear;
  Auth.MySQL56Connection1.connected:=false;
  if MainMenu_Form.ListBox1.Count=0 then
  begin
    MainMenu_Form.Listbox1.Items.Add('Доступных тестов нет');
    MainMenu_Form.Listbox1.enabled:=false;
  end;
  MainMenu_Form.Open_Test.Enabled:=false;
end;

function resourceVersionInfo: string;

(* Unlike most of AboutText (below), this takes significant activity at run-    *)
(* Timer to extract version/release/build numbers from resource information      *)
(* appended to the binary.                                                      *)

var     Stream: TResourceStream;
        vr: TVersionResource;
        fi: TVersionFixedInfo;

begin
  result:='';
  try

(* This raises an exception if version info has not been incorporated into the  *)
(* binary (Lazarus Project -> Project Options -> Version Info -> Version        *)
(* numbering).                                                                  *)

    Stream:= TResourceStream.CreateFromID(HINSTANCE, 1, PChar(RT_VERSION));
    try
      vr:= TVersionResource.Create;
      try
        vr.SetCustomRawDataStream(Stream);
        fi:= vr.FixedInfo;
        Result:=IntToStr(fi.FileVersion[0])+'.'+IntToStr(fi.FileVersion[1])+'.'+
        IntToStr(fi.FileVersion[2])+'.'+IntToStr(fi.FileVersion[3]);
        vr.SetCustomRawDataStream(nil)
      finally
        vr.Free
      end
    finally
      Stream.Free
    end
  except
  end
end { resourceVersionInfo } ;

function OSVersion: string;
var MajVer, MinVer: string;
begin
  {$IFDEF LCLcarbon}
  OSVersion := 'Mac OS X 10.';
  {$ELSE}
  {$IFDEF Linux}
  OSVersion := 'Linux Kernel ';
  {$ELSE}
  {$IFDEF UNIX}
  OSVersion := 'Unix ';
  {$ELSE}
  {$IFDEF WINDOWS}
  if WindowsVersion = wv95 then OSVersion := 'Windows 95 '
   else if WindowsVersion = wvNT4 then OSVersion := 'Windows NT v.4 '
   else if WindowsVersion = wv98 then OSVersion := 'Windows 98 '
   else if WindowsVersion = wvMe then OSVersion := 'Windows ME '
   else if WindowsVersion = wv2000 then OSVersion := 'Windows 2000 '
   else if WindowsVersion = wvXP then OSVersion := 'Windows XP '
   else if WindowsVersion = wvServer2003 then OSVersion := 'Windows Server 2003 '
   else if WindowsVersion = wvVista then OSVersion := 'Windows Vista '
   else if WindowsVersion = wv7 then OSVersion := 'Windows 7 '
   else if WindowsVersion = wv8 then OSVersion := 'Windows 8 '
   else OSVersion:= 'Windows 10 ';
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  {$ENDIF}
  {$IFDEF WINDOWS}
  MajVer := IntToStr(Win32MajorVersion);
  MinVer := IntToStr(Win32MinorVersion);
  {$ELSE}
  MajVer := IntToStr(Lo(DosVersion) - 4);
  MinVer := IntToStr(Hi(DosVersion));
  {$ENDIF}
  OSVersion:=OSVersion+' ('+MajVer+'.'+MinVer+')';
end;

class function TAuth.GetMacAddress : string;

{$IFDEF LINUX}
function GetLinuxMacAddress : string;
const
  linux_path     = '/sys/class/net/%s/address';
  default_device = 'eth0';

var
  AProcess:TProcess;
  AStringList:TStringList;

begin
  AProcess := TProcess.Create(nil);
  AStringList := TStringList.Create;
  AProcess.CommandLine := 'cat /sys/class/net/eth0/address';
  AProcess.Options := AProcess.Options + [poWaitOnExit, poUsePipes];
  AProcess.Execute;
  AStringList.LoadFromStream(AProcess.Output);
  writeln(AStringList.CommaText);
  Aprocess.Destroy;
end.
{$ENDIF}

{$IFDEF DARWIN}
function GetMacOSXMacAddress : string;
var
  theProcess: TProcess;
  theOutput: TStringList;
  i, j, theLine, thePos: integer;

begin
  theProcess := TProcess.Create(nil);
  theProcess.Executable := 'ifconfig';
  theProcess.Parameters.Add('en0');
  theProcess.Options := theProcess.Options + [poWaitOnExit, poUsePipes];
  theProcess.Execute;
  theOutput := TStringList.Create;
  theOutput.LoadFromStream(theProcess.Output);

  theLine := -1;
  for i := 0 to theOutput.Count - 1 do
  begin
    j := pos('ether', theOutput.Strings );
    if (j > 0) then
    begin
      theLine := i;
      thePos := j + length('ether') + 1;
    end;
  end;
  if (theLine > -1) then
    Result := UpperCase(ExtractSubStr(theOutput.Strings[theLine], thePos, [' ']));

  theOutput.Free;
  theProcess.Free;
end;
{$ENDIF}

{$IFDEF WINDOWS}
function GetWinMacAddress: String;
const
  MAX_INTERFACE_NAME_LEN             = $100;
  ERROR_SUCCESS                      = 0;
  MAXLEN_IFDESCR                     = $100;
  MAXLEN_PHYSADDR                    = 8;

  MIB_IF_TYPE_ETHERNET               = 6;

  _MAX_ROWS_ = 20;

type

   MIB_IFROW            = Record
     wszName : Array[0 .. (MAX_INTERFACE_NAME_LEN * 2 - 1)] of char;
     dwIndex              : LongInt;
     dwType               : LongInt;
     dwMtu                : LongInt;
     dwSpeed              : LongInt;
     dwPhysAddrLen        : LongInt;
     bPhysAddr : Array[0 .. (MAXLEN_PHYSADDR-1)] of Byte;
     dwAdminStatus        : LongInt;
     dwOperStatus         : LongInt;
     dwLastChange         : LongInt;
     dwInOctets           : LongInt;
     dwInUcastPkts        : LongInt;
     dwInNUcastPkts       : LongInt;
     dwInDiscards         : LongInt;
     dwInErrors           : LongInt;
     dwInUnknownProtos    : LongInt;
     dwOutOctets          : LongInt;
     dwOutUcastPkts       : LongInt;
     dwOutNUcastPkts      : LongInt;
     dwOutDiscards        : LongInt;
     dwOutErrors          : LongInt;
     dwOutQLen            : LongInt;
     dwDescrLen           : LongInt;
     bDescr     : Array[0 .. (MAXLEN_IFDESCR - 1)] of Char;
     end;

   _IfTable = Record
                 nRows : LongInt;
                 ifRow : Array[1.._MAX_ROWS_] of MIB_IFROW;
              end;

var
   pIfTable  : ^_IfTable;
   TableSize : LongInt;
   tmp       : String;
   i,j       : Integer;
   ErrCode   : LongInt;
begin
   pIfTable := nil;
   //------------------------------------------------------------
   Result := '';
   try
      //-------------------------------------------------------
      // First: just get the buffer size.
      // TableSize returns the size needed.
      TableSize := 0; // Set to zero so the GetIfTabel function
                    // won't try to fill the buffer yet,
                    // but only return the actual size it needs.
      GetIfTable(pIfTable, TableSize, 1);
      if (TableSize < SizeOf(MIB_IFROW) + Sizeof(LongInt)) then
      begin
         Exit; // less than 1 table entry?!
      end; // if-end.

      // Second:
      // allocate memory for the buffer and retrieve the
      // entire table.
      GetMem(pIfTable, TableSize);
      ErrCode := GetIfTable(pIfTable, TableSize, 1);
      if (ErrCode <> ERROR_SUCCESS) then
      begin
         Exit; // OK, that did not work.
               // Not enough memory i guess.
      end; // if-end.

      // Read the ETHERNET addresses.
      for i := 1 to pIfTable^.nRows do
      try
         if (pIfTable^.ifRow[i].dwType=MIB_IF_TYPE_ETHERNET) and
             (pIfTable^.ifRow[i].dwOutOctets <> 0) then
         begin
            tmp := '';
            for j:=0 to pIfTable^.ifRow[i].dwPhysAddrLen-1 do
            begin
               tmp := tmp + format('%.2x:',
                      [ pIfTable^.ifRow[i].bPhysAddr[j] ] );
            end; // for-end.
            //-------------------------------------
            if Length(tmp)>0 then
            begin
              Result := Copy(tmp, 1, Length(tmp) - 1);
              Exit;
            end;
         end; // if-end.
      except
         Exit;
      end; // if-try-except-end.
   finally
      if Assigned(pIfTable) then FreeMem(pIfTable, TableSize);
   end; // if-try-finally-end.
end;
{$ENDIF}


begin
  Result := '';
  {$IFDEF LINUX}
  Result := GetLinuxMacAddress;
  {$ENDIF}
  {$IFDEF DARWIN}
  Result := GetMacOSXMacAddress;
  {$ENDIF}
  {$IFDEF WINDOWS}
  Result := GetWinMacAddress;
  {$ENDIF}
end;

function Pass_Gen(password: string): string;
var i: integer;
begin
  Result:=MD5Print(MD5String(password+MD5Print(MD5String(login))))+MD5Print(MD5String(password));
  for i:=0 to 20 do
  begin
    Result:=MD5Print(MD5String(Result))+Result;
    Result:=MD5Print(MD5String(MD5Print(MD5String(Result))+Result+MD5Print(MD5String(Result))+Result));
    Result:=MD5Print(MD5String(Result+MD5Print(MD5String(Result))+Result+Result));
    Result:=MD5Print(MD5String(Result+MD5Print(MD5String(login))+MD5Print(MD5String(Result))));
  end;
end;

function GetIPFromHost(var HostName, IPaddr, WSAErr: string): Boolean;
type
  Name = array[0..100] of Char;
  PName = ^Name;
var
  HEnt: pHostEnt;
  HName: PName;
  WSAData: TWSAData;
  i: Integer;
begin
  Result := False;
  if WSAStartup($0101, WSAData) <> 0 then begin
    WSAErr := 'Сокет не отвечает!"';
    Exit;
  end;
  IPaddr := '';
  New(HName);
   if GetHostName(HName^, SizeOf(Name)) = 0 then
  begin
    HostName := AnsiToUTF8(StrPas(HName^));
    HEnt := GetHostByName(HName^);
    for i := 0 to HEnt^.h_length - 1 do
     IPaddr :=
      Concat(IPaddr,
      IntToStr(Ord(HEnt^.h_addr_list^[i])) + '.');
    SetLength(IPaddr, Length(IPaddr) - 1);
    Result := True;
  end
  else begin
   case WSAGetLastError of
    WSANOTINITIALISED:WSAErr:='WSANotInitialised';
    WSAENETDOWN      :WSAErr:='WSAENetDown';
    WSAEINPROGRESS   :WSAErr:='WSAEInProgress';
   end;
  end;
  Dispose(HName);
  WSACleanup;
end;

function GetIP:string;
var HTTP: THTTPSend;
    Result1: boolean;
    ss: TStringList;
    s: string;
    i,n: integer;
begin
  HTTP := THTTPSend.Create;
  ss:=TStringList.Create;
  {Http.ProxyHost:='192.168.0.1';   //если соединение не прямое, а через проксю типа UserGate
  http.ProxyPort:='8080';}
  try
    Result1:=HTTP.HTTPMethod('GET', 'http://rl.ammyy.com'); //Запрос к сайту
    if Result1 then
    begin                    //Если ответ есть
      ss.LoadFromStream(HTTP.Document);    //в переменную ss - все строки со страницы
      if ss.Count>=1 then
      begin
        s:=ss[0];
        Result:='';
        n:=0;         //первую строку разбираем, она типа Your IP=83.174.217.211, country = RU
        for i:=1 to Length(s) do
        begin
          if s[i]=',' then
          break;  //если дошли до запятой - стоп
          if n=1 then
          Result:=Result+s[i];
          if s[i]='=' then
          n:=n+1;   //после первого равно начинаем брать
        end;
      end;
    end;
  finally
    HTTP.Free;
    ss.Free;
  end;
end;

function CheckUser:boolean;
var ip_global,ip_local,computer_name,err: string;
begin
  result:=false;
  GetIPFromHost(computer_name,ip_local,err);
  ip_global:=GetIP;
  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
  Auth.SQLQuery1.ExecSQL;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT * FROM users WHERE login=:login');
  Auth.SQLQuery1.Params.ParamByName('login').value:=login;
  Auth.SQLQuery1.Open;
  if UTF16ToUTF8(Trim(Serial))=Auth.SQLQuery1.FieldByName('serial').value then
  result:=true
  else
  if (Auth.GetMacAddress()=Auth.SQLQuery1.FieldByName('mac_reg').value) and
  ((ip_global=Auth.SQLQuery1.FieldByName('ip_global_reg').value) or
  (ip_local=Auth.SQLQuery1.FieldByName('ip_local_reg').value) or
  (computer_name=Auth.SQLQuery1.FieldByName('computer_name_reg').value)) then
  begin
    Auth.MySQL56Connection1.Connected:=false;
    Application.CreateForm(Taccount_confirm, account_confirm);
    account_confirm.show;
    while true do
    begin
      Application.ProcessMessages;
    end;
    if pass_check=false then
    messagedlg('Отмена подтверждения','Подтверждение действия было отменено!',mtwarning,[mbok],0)
    else
    begin
      //user_check:=true;
      account_confirm.release;
    end;
  end
  else
  begin
    messagedlg('Ошибка','Вы зашли с неопознанного компьютера! Данное действие невыполнимо!',mtwarning,[mbok],0)
  end;
end;

function GetSerialNumber(var ASerial: WideString): Boolean;
const
  FlagForwardOnly = $00000020;
  WMIClass = 'Win32_PhysicalMedia';
  WMIProperty = 'SerialNumber';
var
  SWbemLocator, WMIService: Variant;
  WbemObjectSet, WbemObject: Variant;
  EnumVarinat: IEnumvariant;
  SQL: WideString;
begin
  Result := False;
  SWbemLocator := CreateOleObject('WbemScripting.SWbemLocator');
  WMIService := SWbemLocator.ConnectServer('localhost', 'root\CIMV2', '', '');
  SQL := WideString(Format('SELECT %s FROM %s', [WMIProperty, WMIClass]));
  try
    WbemObjectSet := WMIService.ExecQuery(SQL, 'WQL', FlagForwardOnly);
    EnumVarinat := IUnknown(WbemObjectSet._NewEnum) as IEnumVariant;
    if EnumVarinat.Next(1, WbemObject, nil) = 0 then
    begin
      if not VarIsNull(WbemObject.Properties_.Item(WMIProperty).Value) then
        ASerial := WbemObject.Properties_.Item(WMIProperty).Value;
    end;
    WbemObject := Unassigned;
    Result:= True;
  except
    on E: Exception do
      ShowMessage('Невозможно получить HDD s/n: ' + sLineBreak + E.Message);
  end;
end;

procedure TAuth.LoginInClick(Sender: TObject);
var password,ip_global_last,ip_local_last,computer_name_last,err: string;
  check:boolean;
begin
  if (loginedit.text<>'') and (passedit.text<>'') then
  begin
    {GetSerialNumber(serial);
    account_confirm.showmodal;
    showmessage(UTF16ToUTF8(Trim(Serial)));
    check:=CheckUser;
    if Check=false then
    exit;}
    login:=loginedit.text;
    password:=pass_gen(passedit.text);
    loginin.caption:='Вход... Ждите';
    loginin.enabled:=false;
    try
      {MySQL56Connection1.Properties.Values['MYSQL_SSL']     := 'TRUE';
      MySQL56Connection1.Properties.Values['MYSQL_SSL_CA']   := 'c:\Cleardb\ca.pem';
      MySQL56Connection1.Properties.Values['MYSQL_SSL_CERT'] := 'c:\Cleardb\cert.pem';
      MySQL56Connection1.Properties.Values['MYSQL_SSL_KEY']  := 'c:\Cleardb\key-no-password.pem';}
      MySQL56Connection1.Connected:=true;
    except
      messagedlg('Ошибка','Ошибка подключения к БД!'+slinebreak+'Код: 0',mterror,[mbok],0);
      exit;
    end;
    try
      SQLQuery1.SQL.Clear;
      SQLQuery1.SQL.add('SET character_set_client="utf8", character_set_connection="cp1251", character_set_results="utf8"');
      SQLQuery1.ExecSQL;
      SQLQuery1.SQL.Clear;
      SQLQuery1.SQL.add('SELECT login,user_flags,id,user_flags,confirmed,mail,ban_reason,user_group FROM users WHERE login=:login and password=:password');
      SQLQuery1.Params.ParamByName('login').value:=login;
      SQLQuery1.Params.ParamByName('password').value:=password;
      try
        SQLQuery1.Open;
      except
        SQLQuery1.SQL.Clear;
        MySQL56Connection1.Connected:=false;
        messagedlg('Ошибка получения таблицы!',mterror,[mbok],0);
        exit;
      end;
      if SQLQuery1.recordcount=1 then
      begin
        GetIPFromHost(computer_name_last,ip_local_last,err);
        ip_global_last:=GetIP;
        login:=SQLQuery1.FieldByName('login').value;
        user_id:=SQLQuery1.FieldByName('id').AsInteger;
        if SQLQuery1.FieldByName('confirmed').AsInteger=1 then
        begin
          if SQLQuery1.FieldByName('user_flags').value='banned' then
          begin
            messagedlg('Внимание!','Ваш аккаунт заблокирован!'+slinebreak+'Причина: '+
            SQLQuery1.FieldByName('ban_reason').value,mterror,[mbok],0);
            MySQL56Connection1.Connected:=false;
            SQLQuery1.SQL.Clear;
          end
          else
          begin
            Application.CreateForm(TMainMenu_Form, MainMenu_Form);
            Auth.hide;
            //randomize;
            //variant_user:=random(8)+1;
            variant_user:=2;
            error_login_count:=0;
            MainMenu_Form.show;
          end;
        end
        else
        begin
          mail:=SQLQuery1.FieldByName('mail').value;
          Application.CreateForm(Tconfirm_auth, confirm_auth);
          Confirm_auth.mail_confirm_send;
          Auth.hide;
          confirm_auth.show;
          error_login_count:=0;
        end;
        MySQL56Connection1.Connected:=false;
        SQLQuery1.SQL.Clear;
        MyQuery.SQL.Clear;
        MyQuery.SQL.add('INSERT INTO auth_log (user_id,user,ip_global,ip_local,computer_name,mac,OS,serial,variant) VALUES(:user_id,:user,:ip_global,:ip_local,:computer_name,:mac,:OS,:serial,:variant)');
        MyQuery.Params.ParamByName('ip_global').value:=ip_global_last;
        MyQuery.Params.ParamByName('user').value:=login;
        MyQuery.Params.ParamByName('user_id').value:=user_id;
        MyQuery.Params.ParamByName('mac').value:=GetMacAddress();
        MyQuery.Params.ParamByName('OS').value:=OSVersion;
        MyQuery.Params.ParamByName('ip_local').value:=ip_local_last;
        MyQuery.Params.ParamByName('computer_name').value:=computer_name_last;
        //GetSerialNumber(Serial);
        MyQuery.Params.ParamByName('variant').value:=variant_user;
        MyQuery.Params.ParamByName('serial').value:=UTF16ToUTF8(Trim(Serial));
        bd_log;
        loginin.enabled:=true;
        loginin.caption:='Войти';
      end
      else
      begin
        error_login_count:= error_login_count + 1;
        loginin.enabled:=true;
        loginin.caption:='Войти';
        if error_login_count > 2 then
        begin
          loginedit.clear;
          loginedit.enabled:=false;
          passedit.enabled:=false;
          passedit.Clear;
          loginin.Enabled:=false;
          loginin.caption:=inttostr(time_login_count);
          Timer.enabled:=true;
          if (error_login_count mod 5 > 1) and (error_login_count mod 5 < 5)
          then
          begin
            MySQL56Connection1.Connected:=false;
            messagedlg('Вы ошиблись при вводе пароля '+
            inttostr(error_login_count)+' раза. '+
            'Доступ заблокирован на '+inttostr(time_login_count)+
            ' секунд',mtwarning,[mbok],0);
          end
          else
          begin
            MySQL56Connection1.Connected:=false;
            messagedlg('Вы ошиблись при вводе пароля '+
            inttostr(error_login_count)+' раз. Доступ'+' заблокирован на '+
            inttostr(time_login_count)+' секунд',mtwarning,[mbok],0);
          end;
        end
        else
        begin
          MySQL56Connection1.Connected:=false;
          SQLQuery1.SQL.Clear;
          messagedlg('Логин или пароль не найден!',mterror,[mbok],0);
        end;
      end;
    except
      SQLQuery1.SQL.Clear;
      MySQL56Connection1.Connected:=false;
      messagedlg('Ошибка авторизации!'+slinebreak+'Код: 2',mterror,[mbok],0);
      loginin.enabled:=true;
      loginin.caption:='Войти';
    end;
  end
  else
  begin
    messagedlg('Логин или пароль не может быть пустым!',mterror,[mbok],0);
  end;
end;

procedure TAuth.Timer1Timer(Sender: TObject);
const Timer2: integer=1;
begin
  Timer2:=Timer2-1;
  if Timer2=0 then
  begin
    timer1.enabled:=false;
    upd;
  end;
end;

procedure TAuth.TimerTimer(Sender: TObject);
begin
  time_login_count:=time_login_count-1;
  loginin.caption:=inttostr(time_login_count);
  if time_login_count=0 then
  begin
    time_login_count:=10;
    Timer.Enabled:=false;
    loginedit.enabled:=true;
    loginin.enabled:=true;
    passedit.enabled:=true;
    loginin.caption:='Войти';
  end;
end;

procedure TAuth.Reg_buttonClick(Sender: TObject);
begin
  Application.CreateForm(TReg, Reg);
  Reg.show;
  Auth.hide;
  loginedit.clear;
  passedit.clear;
end;

procedure TAuth.FormCreate(Sender: TObject);
begin
  Passedit.passwordchar:=#149;
end;

procedure TAuth.MySQL56Connection1AfterConnect(Sender: TObject);
begin
  Application.ProcessMessages;
  cursor:=crSQLWait;
end;

initialization
  SetResourceStrings(@Translate,nil);

end.
