unit Unit10;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DateTimePicker, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, Menus, ActnList, StdCtrls, Buttons, LCLProc, Clipbrd;

type

  { TTest_redactor_form }

  TTest_redactor_form = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    active_test: TCheckBox;
    CheckBox9: TCheckBox;
    CheckGroup1: TCheckGroup;
    CheckGroups: TCheckGroup;
    CheckGroup2: TCheckGroup;
    ComboBox1: TComboBox;
    DateTimePicker1: TDateTimePicker;
    DateTimePicker2: TDateTimePicker;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label1: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Memo1: TMemo;
    Test_created: TLabel;
    Test_author: TLabel;
    Test_edited: TLabel;
    Label5: TLabel;
    Test_data_edit: TLabel;
    special_info: TLabel;
    Test_name: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    LabeledEdit4: TLabeledEdit;
    LabeledEdit5: TLabeledEdit;
    procedure active_testChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure CheckBox9Change(Sender: TObject);
    procedure CheckGroup2ItemClick(Sender: TObject; Index: integer);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Label10DblClick(Sender: TObject);
    procedure LabeledEdit2Change(Sender: TObject);
    procedure LabeledEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LabeledEdit2KeyPress(Sender: TObject; var Key: char);
    procedure LabeledEdit3Change(Sender: TObject);
    procedure LabeledEdit4Change(Sender: TObject);
    procedure LabeledEdit5Change(Sender: TObject);
    procedure Memo1Enter(Sender: TObject);
    procedure special_infoDblClick(Sender: TObject);
    procedure load_test;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Test_redactor_form: TTest_redactor_form;
  group,name_old,groups_old,special_info_old: string;
  tests_id: array of integer;
  id_last,count_bal: integer;

implementation
uses Unit1,Unit6,Unit8,Unit7;

{$R *.lfm}

{ TTest_redactor_form }

procedure TTest_redactor_form.load_test;
var i,n: integer;
  mark,del,del2: string;
begin
  Button3.Enabled:=false;
  BitBtn2.Enabled:=false;

  // ===* Выводим информацию о тесте *=== \\

  Auth.MySQL56Connection1.Connected:=true;
  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT name,kyrs,mark,user_group,variant,active,DATE_FORMAT(active_to,''%d.%m.%Y''),DATE_FORMAT(active_to,''%H:%i''),DATE_FORMAT(active_from,''%d.%m.%Y''),DATE_FORMAT(active_from,''%H:%i''),author,DATE_FORMAT(created,''%d.%m.%Y %H:%i''),DATE_FORMAT(edited,''%d.%m.%Y %H:%i''),edited_by,special_info,del FROM tests WHERE id=:id_table');
  Auth.SQLQuery1.Params.ParamByName('id_table').value:=id_table;
  try
    Auth.SQLQuery1.Open;
  except
    //log('Test_form - Ошибка отправки запроса.',id_table,id_test,'procedure TTest_form.FormShow(Sender: TObject) - INSERT INTO user_test_log (login,user_id,id_table,action,test_time) VALUES('login','user_id','id_table','action','time')',E.ClassName,E.Message);
    messagedlg('Ошибка!','Ошибка получения таблицы!'+slinebreak+'Попробуйте повторить попытку позже',mterror,[mbok],0);
  end;

  // * Отмеченные курсы * \\
  for i:=1 to 4 do
  if pos(i,Auth.SQLQuery1.FieldByName('kyrs').value)<>0 then
  checkgroup2.checked[i-1]:=true
  else
  checkgroup2.checked[i-1]:=false;

  // * Вариант * \\
  if Auth.SQLQuery1.FieldByName('variant').value<>null then
  begin
    CheckBox9.Checked:=true;
    for i:=1 to 8 do
    if pos(inttostr(i),Auth.SQLQuery1.FieldByName('variant').AsString)<>0 then
    CheckGroup1.checked[i-1]:=true;
  end
  else
  CheckBox9.Checked:=false;

  // * Оценки * \\
  mark:=Auth.SQLQuery1.FieldByName('mark').AsString;
  n:=pos(';',mark);
  LabeledEdit5.Text:=copy(mark,3,2);
  delete(mark,1,n);
  n:=pos(';',mark);
  LabeledEdit4.Text:=copy(mark,3,2);
  delete(mark,1,n);
  n:=pos(';',mark);
  LabeledEdit3.Text:=copy(mark,3,2);
  delete(mark,1,n);
  n:=pos(';',mark);
  LabeledEdit2.Text:=copy(mark,3,2);
  delete(mark,1,n);

  // * Название теста * \\
  Test_name.Text:=Auth.SQLQuery1.FieldByName('Name').AsString;

  // * Активация теста * \\
  del:=Auth.SQLQuery1.FieldByName('del').value;
  del2:=Auth.SQLQuery1.FieldByName('active').value;
  DateTimePicker1.date:=strtodate(Auth.SQLQuery1.FieldByName('DATE_FORMAT(active_to,''%d.%m.%Y'')').AsString);
  DateTimePicker1.Time:=strtotime(Auth.SQLQuery1.FieldByName('DATE_FORMAT(active_to,''%H:%i'')').AsString);
  DateTimePicker2.date:=strtodate(Auth.SQLQuery1.FieldByName('DATE_FORMAT(active_from,''%d.%m.%Y'')').AsString);
  DateTimePicker2.Time:=strtotime(Auth.SQLQuery1.FieldByName('DATE_FORMAT(active_from,''%H:%i'')').AsString);

  // * Кем создан тест * \\
  Test_author.Caption:='Создано: '+Auth.SQLQuery1.FieldByName('author').AsString;

  // * Дата создания теста * \\
  Test_created.Caption:='Дата создания: '+Auth.SQLQuery1.FieldByName('DATE_FORMAT(created,''%d.%m.%Y %H:%i'')').AsString;

  // * Дата редактирования теста * \\
  Test_data_edit.caption:='Дата редактирования: '+Auth.SQLQuery1.FieldByName('DATE_FORMAT(edited,''%d.%m.%Y %H:%i'')').AsString;

  // * Кем отредактирован тест * \\
  Test_edited.Caption:='Отредактировано: '+Auth.SQLQuery1.FieldByName('edited_by').AsString;

  // * Пометки для администраторов * \\
  special_info.caption:='Пометки: '+Auth.SQLQuery1.FieldByName('special_info').AsString;


  // ===* Выводим информацию о заданиях *=== \\

  Auth.SQLQuery1.SQL.Clear;
  Auth.SQLQuery1.SQL.add('SELECT id_test,bal,del FROM test'+inttostr(id_table));
  try
    Auth.SQLQuery1.Open;
  except
    //log('Test_form - Ошибка отправки запроса.',id_table,id_test,'procedure TTest_form.FormShow(Sender: TObject) - INSERT INTO user_test_log (login,user_id,id_table,action,test_time) VALUES('login','user_id','id_table','action','time')',E.ClassName,E.Message);
    messagedlg('Ошибка!','Ошибка получения таблицы!'+slinebreak+'Попробуйте повторить попытку позже',mterror,[mbok],0);
  end;
  ComboBox1.Items.Clear;
  SetLength(tests_id,Auth.SQLQuery1.RecordCount);
  i:=0;
  count_bal:=0;
  while not Auth.SQLQuery1.EOF do
  begin
    if (Auth.SQLQuery1.FieldByName('del').AsString='no') or (user_flags='admin') then
    begin
      count_bal:=count_bal+Auth.SQLQuery1.FieldByName('bal').AsInteger;
      ComboBox1.items.add('Задание №'+Auth.SQLQuery1.FieldByName('id_test').AsString);
      tests_id[i]:=Auth.SQLQuery1.FieldByName('id_test').AsInteger;
      Auth.SQLQuery1.Next;
      i:=i+1;
    end
    else
    Auth.SQLQuery1.Next;
    Label2.caption:='Общее кол-во баллов: '+inttostr(count_bal);
    id_last:=Auth.SQLQuery1.FieldByName('id_test').AsInteger;
  end;
  Auth.MySQL56Connection1.Connected:=false;
  Auth.SQLQuery1.SQL.Clear;

  LabeledEdit2Change(self);
  LabeledEdit3Change(self);
  LabeledEdit4Change(self);
  LabeledEdit5Change(self);

  // ===* Выводим список групп *=== \\
  CheckGroup2ItemClick(self,-1);

  if del='yes' then
  label10.Visible:=true
  else
  label10.Visible:=false;
  if del2='yes' then
  active_test.checked:=true
  else
  active_test.checked:=false;
end;

procedure TTest_redactor_form.Button1Click(Sender: TObject);
var i: integer;
  a,b,c,e,mark,ip_global_last: string;
begin
  if (UTF8Length(Test_name.Text)>=4) and (Test_name.Text<>'    ') then
  begin
  if Button1.Caption='Создать' then
  begin
    try
      Auth.MySQL56Connection1.Connected:=true;
    except
      messagedlg('Ошибка','Ошибка подключения к БД!'+slinebreak+'Код: 0',mterror,[mbok],0);
      exit;
    end;
    try
      a:='';
      c:='';
      mark:='5-'+LabeledEdit5.Text+';4-'+LabeledEdit4.Text+';3-'+LabeledEdit3.Text+';2-'+LabeledEdit2.Text+';';
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('INSERT INTO tests (kyrs,active,Name,user_group,edited,edited_by,special_info,variant,active_to,active_from,mark,author)');
      Auth.SQLQuery1.SQL.add(' VALUES(:kyrs,:active,:name,:user_group,CURRENT_TIMESTAMP,:edited_by,:special_info,:variant,:active_to,:active_from,:mark,:author)');
      c:=special_info.caption;
      Delete(c, 1, 16);
      if c<>'' then
      Auth.SQLQuery1.Params.ParamByName('special_info').value:=c
      else
      Auth.SQLQuery1.Params.ParamByName('special_info').value:=null;
      Auth.SQLQuery1.Params.ParamByName('author').value:=login;
      a:='';
      Auth.SQLQuery1.Params.ParamByName('edited_by').value:=login;
      Auth.SQLQuery1.Params.ParamByName('name').value:=Test_name.text;
      if active_test.checked=true then
      Auth.SQLQuery1.Params.ParamByName('active').value:='yes'
      else
      Auth.SQLQuery1.Params.ParamByName('active').value:='no';
      for i:=0 to 3 do
      if checkgroup2.Checked[i]=true then
      a:=a+checkgroup2.items[i]+', ';
      SetLength(a,Length(a)-2);
      Auth.SQLQuery1.Params.ParamByName('kyrs').value:=a;
      b:='';
      for i:=checkgroups.items.Count-1 downto 0 do
      begin
        if checkgroups.Checked[i]=true then
        b:=b+checkgroups.items[i]+', ';
      end;
      SetLength(b,Length(b)-2);
      Auth.SQLQuery1.Params.ParamByName('user_group').value:=b;
      e:='';
      if CheckGroup1.Enabled=true then
      for i:=1 to 8 do
      begin
        if CheckGroup1.Checked[i-1]=true then
        e:=inttostr(i)+','+e;
        if i=8 then
        SetLength(e,Length(e)-1);
      end;
      if e='' then
      Auth.SQLQuery1.Params.ParamByName('variant').value:=null
      else
      Auth.SQLQuery1.Params.ParamByName('variant').value:=e;
      Auth.SQLQuery1.Params.ParamByName('mark').value:=mark;
      Auth.SQLQuery1.Params.ParamByName('active_to').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker1.DateTime);
      Auth.SQLQuery1.Params.ParamByName('active_from').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker2.DateTime);
      Auth.MyQuery.SQL.Clear;
      Auth.MyQuery.SQL.add('INSERT INTO table_log (user_id,ip_global,groups_change,name_change,special_info,kyrs_change,active_change,id_table,action,variant,active_to,active_from,mark)');
      Auth.MyQuery.SQL.add(' VALUES(:user_id,:ip_global,:groups_change,:name_change,:special_info,:kyrs_change,:active_change,:id_table,:action,:variant,:active_to,:active_from,:mark)');
      ip_global_last:=GetIP;
      Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global_last;
      Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
      Auth.MyQuery.Params.ParamByName('name_change').value:=Test_name.text;
      Auth.MyQuery.Params.ParamByName('groups_change').value:=b;
      Auth.MyQuery.Params.ParamByName('special_info').value:=c;
      if active_test.checked=true then
      Auth.MyQuery.Params.ParamByName('active_change').value:='Тест активирован'
      else
      Auth.MyQuery.Params.ParamByName('active_change').value:='Тест деактивирован';
      Auth.MyQuery.Params.ParamByName('kyrs_change').value:=a;
      Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
      Auth.MyQuery.Params.ParamByName('mark').value:=mark;
      Auth.MyQuery.Params.ParamByName('action').value:='Создание';
      Auth.MyQuery.Params.ParamByName('variant').value:=Auth.SQLQuery1.Params.ParamByName('variant').value;
      Auth.MyQuery.Params.ParamByName('active_to').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker1.DateTime);
      Auth.MyQuery.Params.ParamByName('active_from').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker2.DateTime);
      bd_log;
      Auth.SQLQuery1.ExecSQL;
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('CREATE  TABLE  `pixeldeveloperteam`.`test'+inttostr(id_table)+'` (`id_test` int(11) NOT NULL,`test_type` varchar(10) NOT NULL,`text` varchar( 512  )  NOT  NULL ,`answer1` varchar( 64  )  NOT  NULL ,`answer2` varchar( 64  )  NOT  NULL ,`answer3` varchar( 64  )  DEFAULT NULL ,`answer4` varchar( 64  )  DEFAULT NULL ,`answer5` varchar( 64  )  DEFAULT NULL ,`answer6` varchar( 64  )  DEFAULT NULL ,`answer7` varchar( 64  )  DEFAULT NULL ,`answer8` varchar( 64  )  DEFAULT NULL ,`good_answer` varchar( 23  )  NOT  NULL ,`bal` int( 2  )  NOT  NULL DEFAULT  ''1'', `time` TIME NULL ,`del` varchar( 3  )  NOT  NULL DEFAULT  ''no'') ENGINE  = InnoDB  DEFAULT CHARSET  = utf8;');
      Auth.SQLQuery1.ExecSQL;
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('ALTER TABLE `pixeldeveloperteam`.`test'+inttostr(id_table)+'` ADD PRIMARY KEY(`id_test`), MODIFY `id_test` int(11) NOT NULL AUTO_INCREMENT;');
      Auth.SQLQuery1.ExecSQL;
      Auth.MySQL56Connection1.Connected:=false;
      Button1.Caption:='Сохранить';
      ComboBox1.Enabled:=true;
      BitBtn1.Enabled:=true;
      active_test.Enabled:=true;
      Button3.Enabled:=true;
      Button2.Enabled:=true;
      BitBtn2.Enabled:=true;
    except
      Auth.MySQL56Connection1.Connected:=false;
      Auth.SQLQuery1.SQL.Clear;
      messagedlg('Ошибка','Ошибка сохранения, повторите попытку!',mterror,[mbok],0);
    end;
  load_test;
  end
  else
  begin
    try
      Auth.MySQL56Connection1.Connected:=true;
    except
      messagedlg('Ошибка','Ошибка подключения к БД!'+slinebreak+'Код: 0',mterror,[mbok],0);
      exit;
    end;
    try
      a:='';
      c:='';
      mark:='5-'+LabeledEdit5.Text+';4-'+LabeledEdit4.Text+';3-'+LabeledEdit3.Text+';2-'+LabeledEdit2.Text+';';
      Auth.SQLQuery1.SQL.Clear;
      Auth.SQLQuery1.SQL.add('UPDATE tests SET kyrs=:kyrs,active=:active,Name=:name,user_group=:user_group,edited=CURRENT_TIMESTAMP,edited_by=:edited_by,special_info=:special_info,variant=:variant,active_to=:active_to,active_from=:active_from,mark=:mark WHERE id=:id_table');
      Auth.SQLQuery1.Params.ParamByName('id_table').value:=id_table;
      c:=special_info.caption;
      Delete(c, 1, 16);
      Auth.SQLQuery1.Params.ParamByName('special_info').value:=c;
      a:='';
      Auth.SQLQuery1.Params.ParamByName('edited_by').value:=login;
      Auth.SQLQuery1.Params.ParamByName('name').value:=Test_name.text;
      if active_test.checked=true then
      Auth.SQLQuery1.Params.ParamByName('active').value:='yes'
      else
      Auth.SQLQuery1.Params.ParamByName('active').value:='no';
      for i:=0 to 3 do
      if checkgroup2.Checked[i]=true then
      a:=a+checkgroup2.items[i]+', ';
      SetLength(a,Length(a)-2);
      Auth.SQLQuery1.Params.ParamByName('kyrs').value:=a;
      b:='';
      for i:=checkgroups.items.Count-1 downto 0 do
      begin
        if checkgroups.Checked[i]=true then
        b:=b+checkgroups.items[i]+', ';
      end;
      SetLength(b,Length(b)-2);
      Auth.SQLQuery1.Params.ParamByName('user_group').value:=b;
      e:='';
      if CheckGroup1.Enabled=true then
      for i:=1 to 8 do
      begin
        if CheckGroup1.Checked[i-1]=true then
        e:=inttostr(i)+','+e;
        if i=8 then
        SetLength(e,Length(e)-1);
      end;
      if e='' then
      Auth.SQLQuery1.Params.ParamByName('variant').value:=null
      else
      Auth.SQLQuery1.Params.ParamByName('variant').value:=e;
      Auth.SQLQuery1.Params.ParamByName('mark').value:=mark;
      Auth.SQLQuery1.Params.ParamByName('active_to').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker1.DateTime);
      Auth.SQLQuery1.Params.ParamByName('active_from').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker2.DateTime);
      Auth.MyQuery.SQL.Clear;
      Auth.MyQuery.SQL.add('INSERT INTO table_log (user_id,ip_global,groups_change,name_change,special_info,kyrs_change,active_change,id_table,action,variant,active_to,active_from,mark)');
      Auth.MyQuery.SQL.add(' VALUES(:user_id,:ip_global,:groups_change,:name_change,:special_info,:kyrs_change,:active_change,:id_table,:action,:variant,:active_to,:active_from,:mark)');
      ip_global_last:=GetIP;
      Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global_last;
      Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
      Auth.MyQuery.Params.ParamByName('name_change').value:=Test_name.text;
      Auth.MyQuery.Params.ParamByName('groups_change').value:=b;
      Auth.MyQuery.Params.ParamByName('special_info').value:=c;
      if active_test.checked=true then
      Auth.MyQuery.Params.ParamByName('active_change').value:='Тест активирован'
      else
      Auth.MyQuery.Params.ParamByName('active_change').value:='Тест деактивирован';
      Auth.MyQuery.Params.ParamByName('kyrs_change').value:=a;
      Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
      Auth.MyQuery.Params.ParamByName('mark').value:=mark;
      Auth.MyQuery.Params.ParamByName('action').value:='Редактирование';
      Auth.MyQuery.Params.ParamByName('variant').value:=Auth.SQLQuery1.Params.ParamByName('variant').value;
      Auth.MyQuery.Params.ParamByName('active_to').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker1.DateTime);
      Auth.MyQuery.Params.ParamByName('active_from').value:=FormatDateTime('yyyy-mm-dd hh:mm',DateTimePicker2.DateTime);
      bd_log;
      Auth.SQLQuery1.ExecSQL;
      Auth.MySQL56Connection1.Connected:=false;
      Auth.SQLQuery1.SQL.Clear;
    except
      Auth.MySQL56Connection1.Connected:=false;
      Auth.SQLQuery1.SQL.Clear;
      messagedlg('Ошибка','Ошибка сохранения, повторите попытку!',mterror,[mbok],0);
    end;
  load_test;
  end;
  end
  else
  messagedlg('Ошибка сохранения!','Название не может состоять только из пробелов или иметь длину меньше 4 символов!',mterror,[mbok],0);
end;

procedure TTest_redactor_form.BitBtn2Click(Sender: TObject);
var ip_global: string;
  i: integer;
begin
  if ComboBox1.ItemIndex=0 then
  messagedlg('Удаление невозможно','Нельзя удалить задание №1',mterror,[mbok],0)
  else
  if messagedlg('Удалить задание?','Вы действительно хотите удалить задание?'+
  slinebreak+'Действие отменить будет невозможно!',mtwarning,[mbyes,mbno],0)=mryes then
  begin
    id_test:=tests_id[ComboBox1.ItemIndex];
    Auth.MyQuery.SQL.Clear;
    Auth.MyQuery.SQL.add('INSERT INTO test_log (user_id,ip_global,test_id,id_table,action) VALUES(:user_id,:ip_global,:test_id,:id_table,:action)');
    ip_global:=GetIP;
    Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
    Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
    Auth.MyQuery.Params.ParamByName('id_test').value:=id_test;
    Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
    Auth.MyQuery.Params.ParamByName('action').value:='Удаление задания';
    bd_log;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('UPDATE test'+inttostr(id_table)+' SET del=:del WHERE id_test=:id_test');
    Auth.SQLQuery1.Params.ParamByName('del').value:='yes';
    Auth.SQLQuery1.Params.ParamByName('id_test').value:=id_test;
    Auth.SQLQuery1.ExecSQL;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    for i:=ComboBox1.ItemIndex to Length(tests_id)-1 do
    tests_id[i-1]:=tests_id[i];
    SetLength(tests_id,Length(tests_id)-1);
    ComboBox1.Items.Delete(ComboBox1.ItemIndex);
    messagedlg('Задание удалено!','Задание было успешно удалено!',mtcustom,[mbok],0);
    load_test;
  end;
end;

procedure TTest_redactor_form.BitBtn1Click(Sender: TObject);
begin
  id_test:=id_last+1;
  application.CreateForm(TRedactor_form,Redactor_form);
  Redactor_form.Button1.caption:='Создать';
  Redactor_form.show;
  Test_redactor_form.Hide;
end;

procedure TTest_redactor_form.active_testChange(Sender: TObject);
begin
  if active_test.Checked=true then
  begin
    if ComboBox1.Items.Count<5 then
    begin
      active_test.Checked:=false;
      messagedlg('Ошибка активации','Активировать этот тест невозможно, пока он содержит менее 5 заданий!',
      mterror,[mbok],0);
    end;
    if Label10.Visible=true then
    begin
      active_test.Checked:=false;
      messagedlg('Ошибка активации','Активировать этот тест невозможно, т.к. он был удален',
      mterror,[mbok],0);
    end;
  end;
end;

procedure TTest_redactor_form.Button3Click(Sender: TObject);
begin
  id_test:=tests_id[ComboBox1.ItemIndex];
  application.CreateForm(TRedactor_form,Redactor_form);
  Redactor_form.show;
  Test_redactor_form.Hide;
end;

procedure TTest_redactor_form.CheckBox9Change(Sender: TObject);
begin
  if CheckBox9.Checked=true then
  CheckGroup1.enabled:=true
  else
  CheckGroup1.enabled:=false;
end;

procedure TTest_redactor_form.CheckGroup2ItemClick(Sender: TObject;
  Index: integer);
var i: integer;
begin
  Auth.MySQL56Connection1.Connected:=true;
  checkgroups.Items.Clear;
  for i:=0 to 3 do
  if checkgroup2.Checked[i]=true then
  begin
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('SELECT * FROM groups WHERE kyrs=:kyrs');
    Auth.SQLQuery1.Params.ParamByName('kyrs').value:=i+1;
    Auth.SQLQuery1.Open;
    while not Auth.SQLQuery1.EOF do
    begin
      checkgroups.Items.add(Auth.SQLQuery1.FieldByName('group').value);
      Auth.SQLQuery1.Next;
    end;
  end;
  begin
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('SELECT * FROM tests WHERE id=:id_table');
    Auth.SQLQuery1.Params.ParamByName('id_table').value:=id_table;
    Auth.SQLQuery1.Open;
    for i:=0 to CheckGroups.items.count-1 do
    begin
      if pos(CheckGroups.items[i],Auth.SQLQuery1.FieldByName('user_group').AsString)<>0 then
      CheckGroups.checked[i]:=true;
    end;
  end;
  Auth.SQLQuery1.SQL.Clear;
  Auth.MySQL56Connection1.Connected:=false;
end;

procedure TTest_redactor_form.ComboBox1Change(Sender: TObject);
begin
  if ComboBox1.ItemIndex=-1 then
  begin
    Button3.Enabled:=false;
    BitBtn2.Enabled:=false;
  end
  else
  begin
    Button3.Enabled:=true;
    BitBtn2.Enabled:=true;
  end;
end;

procedure TTest_redactor_form.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  cp_form.show;
end;

procedure TTest_redactor_form.FormShow(Sender: TObject);
begin
  if Button1.Caption='Сохранить' then
  load_test
  else
  begin
    Button2.Enabled:=false;
    ComboBox1.Enabled:=false;
    BitBtn1.Enabled:=false;
    active_test.Enabled:=false;
    Button3.Enabled:=false;
    BitBtn2.Enabled:=false;
  end;
end;

procedure TTest_redactor_form.Label10DblClick(Sender: TObject);
var ip_global: string;
begin
  if messagedlg('Восстановить тест?','Вы действительно хотите восстановить данный тест?',mtinformation,
  [mbyes,mbno],0)=mryes then
  begin
    Auth.MyQuery.SQL.Clear;
    Auth.MyQuery.SQL.add('INSERT INTO table_log (user_id,ip_global,id_table,action) VALUES(:user_id,:ip_global,:id_table,:action)');
    ip_global:=GetIP;
    Auth.MyQuery.Params.ParamByName('ip_global').value:=ip_global;
    Auth.MyQuery.Params.ParamByName('user_id').value:=user_id;
    Auth.MyQuery.Params.ParamByName('id_table').value:=id_table;
    Auth.MyQuery.Params.ParamByName('action').value:='Восстановление';
    bd_log;
    Auth.MySQL56Connection1.Connected:=true;
    Auth.SQLQuery1.SQL.Clear;
    Auth.SQLQuery1.SQL.add('UPDATE tests SET del=:del WHERE id=:id');
    Auth.SQLQuery1.Params.ParamByName('del').value:='no';
    Auth.SQLQuery1.Params.ParamByName('id').value:=id_table;
    Auth.SQLQuery1.ExecSQL;
    Auth.SQLQuery1.SQL.Clear;
    Auth.MySQL56Connection1.Connected:=false;
    Label10.Visible:=false;
  end;
end;

procedure TTest_redactor_form.LabeledEdit2Change(Sender: TObject);
var proc,bal: real;
begin
  if LabeledEdit2.Text<>'' then
  begin
    proc:=strtofloat(LabeledEdit2.Text);
    bal:=count_bal;
    proc:=round(bal*proc/100);
    Label4.Caption:=floattostr(proc)+' баллов';
  end;
end;

procedure TTest_redactor_form.LabeledEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if ((ssCtrl in Shift) AND (Key = ord('V'))) or (Key = 45) then
  begin
    if Clipboard.HasFormat(CF_TEXT) then
    ClipBoard.Clear;
    Key := 0;
  end;
end;

procedure TTest_redactor_form.LabeledEdit2KeyPress(Sender: TObject;
  var Key: char);
begin
  if not (key in ['0'.. '9',#8]) then
  key:= #0;
end;

procedure TTest_redactor_form.LabeledEdit3Change(Sender: TObject);
var proc,bal: real;
begin
  if LabeledEdit3.Text<>'' then
  begin
    proc:=strtofloat(LabeledEdit3.text);
    bal:=count_bal;
    proc:=round(bal*proc/100);
    Label6.Caption:=floattostr(proc)+' баллов';
  end;
end;

procedure TTest_redactor_form.LabeledEdit4Change(Sender: TObject);
var proc,bal: real;
begin
  if LabeledEdit4.Text<>'' then
  begin
    proc:=strtofloat(LabeledEdit4.Text);
    bal:=count_bal;
    proc:=round(bal*proc/100);
    Label7.Caption:=floattostr(proc)+' баллов';
  end;
end;

procedure TTest_redactor_form.LabeledEdit5Change(Sender: TObject);
var proc,bal: real;
begin
  if (LabeledEdit5.Text<>'') then
  begin
    proc:=strtofloat(LabeledEdit5.Text);
    bal:=count_bal;
    proc:=round(bal*proc/100);
    Label8.Caption:=floattostr(proc)+' баллов';
  end;
end;

procedure TTest_redactor_form.Memo1Enter(Sender: TObject);
begin
  Memo1.Visible:=false;
  special_info.caption:='Пометки: '+memo1.Text;
  special_info.Enabled:=true;
end;

procedure TTest_redactor_form.special_infoDblClick(Sender: TObject);
var s: string;
begin
  Memo1.visible:=true;
  special_info.Enabled:=false;
  s:=special_info.caption;
  Delete(s, 1, 16);
  memo1.Text:=s;
  memo1.SetFocus;
end;

end.

